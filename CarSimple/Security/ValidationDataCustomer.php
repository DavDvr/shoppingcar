<?php


namespace App\Security;


use App\Connect\Database;
use App\Models\Customers;
use App\Dao\{DaoCustomers};
use PDO;

class ValidationDataCustomer
{
    private $password;
    private $daoCustom;
    private $confirmationPassword;
    public function __construct($password=null,$confirmationPassword=null)
    {
        $this->password= $password;
        $this->daoCustom = new DaoCustomers();
        $this->confirmationPassword=$confirmationPassword;
    }

    /**
     * @param null $dataCustomer
     * @return Customers|mixed
     * recup data for inscription
     */
    public function recupDataCustomer($dataCustomer=null){

        $lastname = filter_input(INPUT_POST, 'lastname');
        $firstname = filter_input(INPUT_POST, 'firstname');
        $birthdate= filter_input(INPUT_POST, 'birthdate');
        $licencenumber = filter_input(INPUT_POST, 'licencenumber');
        $address =filter_input(INPUT_POST, 'address');
        $this->password = filter_input(INPUT_POST,'password');
        $passwordHash = password_hash( $this->password, PASSWORD_ARGON2I);
        $email = filter_input(INPUT_POST, 'email');
        $role = ['ROLE_USER'];

        if($dataCustomer != null){
            $dataCustomer->setLastName($lastname);
            $dataCustomer->setFirstName($firstname);
            $dataCustomer->setBirthdate($birthdate);
            $dataCustomer->setAddress($address);
            $dataCustomer->setLicenceNumber($licencenumber);
            $dataCustomer->setPassword($passwordHash);
            $dataCustomer->setEmail($email);
            $dataCustomer->setRole($role);

            return $dataCustomer;
        }else{

            $customer = new Customers(null, $lastname, $firstname, $birthdate, $address, $licencenumber, $passwordHash, $email, $role);
            return $customer;
        }
    }


    /**
     * @param null $dataCustomer
     * check data for inscription
     */
    public function isValidDataInscription($dataCustomer=null){
        $error =[];

        if( empty($dataCustomer->getLastName()) || mb_strlen($dataCustomer->getLastName()) < 2 || mb_strlen($dataCustomer->getLastName()) > 60 || !preg_match("#^[a-zA-Z-]*$#", $dataCustomer->getLastName())){

            $error[] = "The name isn't correct, you must enter a name between 2 and 60 character";
        }

        if(empty($dataCustomer->getFirstName()) || mb_strlen($dataCustomer->getFirstName())< 2 || mb_strlen($dataCustomer->getFirstName()) > 60 || !preg_match("#^[a-zA-Z-]*$#", $dataCustomer->getFirstName())){

            $error[]= "The firstname isn't correct, tou must enter a firstname between 2 and 60 character";
        }

        if(empty($dataCustomer->getBirthdate()) || !preg_match('#^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$#', $dataCustomer->getBirthdate())){
            $error[]= "The date isn't correct, you must enter a french date";
        }

        if(empty($dataCustomer->getLicenceNumber()) || !preg_match("#^[0-9]{12}$#", $dataCustomer->getLicenceNumber())){
            $error[]= "The licence number isn't correct";
        }

        if(!preg_match('/^[a-zA-Z0-9àáâãäåçèéêëìíîïðòóôõöùúûüýÿÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÐÒÓÔÕÖÙÚÛÜÝŸ\.\/_,;+=%€@\!:\^\s\n\t\?\r\'-]+$/',$dataCustomer->getAddress()) || empty($dataCustomer->getAddress())){

            $error[]= "The address isn't correct";
        }

        if(!filter_var($dataCustomer->getEmail(), FILTER_VALIDATE_EMAIL) || empty($dataCustomer->getEmail()) || $this->customerExistInBDD($dataCustomer->getEmail()) === true){

            $error[]= "The address email isn't correct or you have already a account";
        }
        if(empty($this->password ) || mb_strlen($this->password)< 4 || mb_strlen($this->password) >50){

            $error[]= "The password must be between 4 and 50 characters";
        }

        return $error;
    }

    /**
     * @param null $name
     * @return bool
     * search a customer for not store the same in BDD
     */
    public function customerExistInBDD($login){

        $pdo = Database::connect();
        $req = "SELECT email FROM customers where email= :email";
        $stat= $pdo->prepare($req);
        $stat->execute([":email"=>$login]);
        $result = $stat->fetch(\PDO::FETCH_OBJ);

        if($result){
            return true;
        }else{

            return false;
        }

    }

    /**
     * @param $email
     * @param $password
     * @return bool
     * search a customer in BDD
     */
    public function searchAndValidateCustomer($email, $password){
        $pdo = Database::connect();
        $req = "select * from customers where email= :email";
        $stat= $pdo->prepare($req);
        $stat->execute([":email"=>$email]);
        $customer =$stat->fetch(PDO::FETCH_OBJ);

        if($customer && password_verify($password, $customer->passwordCustomer)){
            return true;
        }else{
            return false;
        }
    }

    //---------------------------------------------------------------------------------- For the login

    /**
     * @param null $customer
     * @return Customers|mixed
     * recup data for login
     */
    public function recupDataForLogin($customer = null){
        $error=[];
        $emailLogin = filter_input(INPUT_POST, 'emailLogin');
        $passwordLogin = filter_input(INPUT_POST, 'passwordLogin');

        if($customer != null){

            $customer->setEmail($emailLogin);
            $customer->setPassword($passwordLogin);

            return $customer;
        }else{
            $cust = new Customers(null,null, null,null,null,null,$passwordLogin,$emailLogin,null);
            return $cust;
        }

    }


    /**
     * @param null $customer
     * @return array
     * check if data for login
     */
    public function validDataFormLogin($customer =null){
        $error =[];

        if(!filter_var($customer->getEmail(), FILTER_VALIDATE_EMAIL) || empty($customer->getEmail())){

            $error[]= "The address email isn't correct";
        }

        if($customer->getPassword() === "" || mb_strlen($customer->getPassword())< 4 || mb_strlen($customer->getPassword()) > 100 ){
            $error[]= "Your password isn't correct, you must respect the number character between 4 and 100 please ";
        }

        if($customer->getPassword() !==  $this->confirmationPassword = filter_input(INPUT_POST, 'confirmationPassword')){
            $error[]= "Your passwords isn't the same";
        }

        if (!$this->daoCustom->findByEmailAndPassword($customer->getEmail(),$customer->getPassword())){
            $error[]= "There not account with this firstname and password";
        }

        return $error;
    }

    /**
     * @param $file
     * function of debug
     */
    public function debug($file){
        echo "<pre>";
        echo var_dump($file);
        echo "</pre>";
        exit();
    }

}