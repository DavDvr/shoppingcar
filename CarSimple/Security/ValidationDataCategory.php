<?php


namespace App\Security;
use App\Models\{Category};
use App\Connect\{Database};

class ValidationDataCategory
{
    public function recupDataCategory($cat =null){

        $nameCat = filter_input(INPUT_POST,'nameCat');
        $descriptionCat = filter_input(INPUT_POST, 'descriptionCat');

        if($cat != null){
            $cat->setNameCategory($nameCat);
            $cat->setDescription($descriptionCat);

            return $cat;
        }else{
            $cat = new Category(null,$nameCat,$descriptionCat);
            return $cat;
        }

    }

    public function validDataCategory($cat=null){
        $error = [];

        if(empty($cat->getNameCategory()) || mb_strlen($cat->getNameCategory()) < 3 || mb_strlen($cat->getNameCategory()) > 200 || !preg_match('#[A-Za-z0-9-]#', $cat->getNameCategory())){

            $error[] = "The category isn't correct, you must enter a category between 5 and 50 characters";

        }

        if(empty($cat->getDescription()) || mb_strlen($cat->getDescription()) < 10 ||  mb_strlen($cat->getDescription()) > 1500){

            $error[] = "The description isn't correct, you must enter a description with min 10 et max 1500 characters";
        }

        if($this->CatExistInBDD($cat->getNameCategory())){
            $error[] = "The category already exist in the database of category";
        }

        return $error;

    }

    /**
     * @param null $name
     * @return bool
     * search a cat in BDD for not store the same car
     */
    public function CatExistInBDD($name){

        $pdo = Database::connect();
        $req = "SELECT * FROM category where name= :name";
        $stat= $pdo->prepare($req);
        $stat->execute([":name"=>$name]);
        $result = $stat->fetch(\PDO::FETCH_OBJ);

        if($result){
            return true;
        }else{

            return false;
        }

    }

    public function debug($variable){
        echo " <pre>";
       echo var_dump($variable);
       echo "</pre>";
       exit();
    }
}