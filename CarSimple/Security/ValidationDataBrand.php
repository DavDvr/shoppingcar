<?php


namespace App\Security;
use App\Connect\Database;
use App\Models\{CarBrand};

class ValidationDataBrand
{

    public function recupDataBrand($dataBrand=null){

       $brand = trim(filter_input(INPUT_POST,'brand'));
       $manufacturing_place = trim(filter_input(INPUT_POST,'manufacturing_place'));
       $controlquality = filter_input(INPUT_POST, 'controlquality');
       $description = trim(filter_input(INPUT_POST, 'description'));

        if($dataBrand != null){
            $dataBrand->setName($brand);
            $dataBrand->setDescription($description);
            $dataBrand->setCarManufacturingPlace($manufacturing_place);
            $dataBrand->setControlQuality($controlquality);

            return $dataBrand;
        }else{
            $brandData = new CarBrand(null,$brand,$description,$manufacturing_place,$controlquality);
            return $brandData;
        }
    }


    public function isValidBrandData($dataBrand=null){
        $error =[];

        if(empty($dataBrand->getName()) || mb_strlen($dataBrand->getName()) < 2 || mb_strlen($dataBrand->getName()) > 25 || !preg_match("#^[a-zA-Z0-9-]*$#", $dataBrand->getName())){

            $error[] = "The name isn't correct, you must enter a name between 2 and 25 characters";
        }

        if($this->searchAndValidateBrand($dataBrand->getName())){
            $error[]= "This brand is already in your database ";
        }

        if(empty($dataBrand->getDescription()) || mb_strlen($dataBrand->getDescription()) < 15 || mb_strlen($dataBrand->getDescription()) > 2500 ){

            $error[]= "The description isn't correct, you must enter a description between 15 and 2500 characters";
        }

        if(empty($dataBrand->getCarManufacturingPlace()) || mb_strlen($dataBrand->getCarManufacturingPlace()) < 4 || mb_strlen($dataBrand->getCarManufacturingPlace()) > 40 || !preg_match("#^[a-zA-Z0-9-]*$#", $dataBrand->getCarManufacturingPlace())){

            $error[]= "The manufacturing place isn't correct, she must be between 4 and 40 characters";
        }

        if($dataBrand->getControlQuality() === "" || !preg_match("#^[0-1]*$#", $dataBrand->getControlQuality())){

            $error[] = "The value of control quality isn't correct, you must enter a value";
        }


        return $error;
    }

    /**
     * @param $email
     * @param $password
     * @return bool
     * search a car in BDD for not store the same car
     */
    public function searchAndValidateBrand($name){
        $pdo = Database::connect();
        $req = "select * from carbrand where name= :name";
        $stat= $pdo->prepare($req);
        $stat->execute([":name"=>$name]);
        $brand =$stat->fetch(\PDO::FETCH_OBJ);

        if($brand){
            return true;
        }else{
            return false;
        }
    }


    /**
     * @param $file
     * function of debug
     */
    public function debug($file){
        echo "<pre>";
        echo var_dump($file);
        echo "</pre>";
        exit();
    }

}