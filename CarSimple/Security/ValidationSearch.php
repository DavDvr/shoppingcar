<?php


namespace App\Security;

use App\Connect\Database;
use App\Models\Cars;
use PDO;

class ValidationSearch
{
    public function __construct()
    {
    }

    public function recupDataSearch($dataSearch = null){

     $dataSearch= trim(filter_input(INPUT_POST,'searchCar'));

     if($dataSearch != null){
         return $dataSearch;
     }

    }

    public function validationFormSearch($dataSearch= null){

        $error=[];
        if(mb_strlen($dataSearch) <0 || mb_strlen($dataSearch) > 200 || !preg_match('#[A-Za-z0-9-_\\.\(\),]*#',$dataSearch)){
            $error[] = "Your search isn't correct, you cannot enter a script here";
        }
        return $error;
    }

    function searchAllBeginBy($datasearch)
    {
        try {

            $pdo = Database::connect();
            $req = "select * from CARS where name LIKE'".$datasearch."%' ORDER BY entryDate";
            $stat = $pdo->query($req);
            $tabCars = $stat->fetchAll(PDO::FETCH_ASSOC);

            $tablAllCars = [];

            foreach($tabCars  as $tabCar){
                $allCars = new Cars();

                $allCars->setId($tabCar['id']);
                $allCars->setName($tabCar['name']);
                $allCars->setNumberPlace($tabCar['numberPlace']);
                $allCars->setPower($tabCar['power']);
                $allCars->setTypeFuel($tabCar['typeFuel']);
                $allCars->setColor($tabCar['color']);
                $allCars->setImmatriculation($tabCar['immatriculation']);
                $allCars->setCarTire($tabCar['carTire']);
                $allCars->setPowerSterring($tabCar['powerSterring']);
                $allCars->setNbrKilometres($tabCar['nbrKilometres']);
                $allCars->setPrice($tabCar['price']);
                $allCars->setIdCat($tabCar['idCat']);
                $allCars->setIdBrand($tabCar['idBrand']);
                $allCars->setNameFile($tabCar['nameFile']);
                $allCars->setFile($tabCar['file']);

                $tablAllCars[] = $allCars;
            }

            //i return this tab
            return $tablAllCars;

        }catch (\Exception $excep){

            return 'Just a litle problem with the function findAll: '.$excep->getMessage();
        }

    }

    /**
     * @param $file
     * function of debug
     */
    public function debug($file){
        echo "<pre>";
        echo var_dump($file);
        echo "</pre>";
        exit();
    }
}