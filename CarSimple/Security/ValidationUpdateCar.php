<?php


namespace App\Security;


use App\Connect\Database;
use App\Controllers\CarsController;
use App\Models\Cars;

class ValidationUpdateCar
{
    private $file_destination;
    public function __construct($file_destination=null){
        $this->file_destination = $file_destination;
    }

    /**
     * @param null $dataCar
     * @return Cars|mixed
     * the function allow to pick up my data for my car
     */
    public function recupDataUpdateCar($dataCar=null){

        $controllerCar = new CarsController();

        $nameCar = trim(filter_input(INPUT_POST, 'nameCar'));
        $nbrPlaceCar = trim(filter_input(INPUT_POST, 'nbrPlaceCar'));
        $powerCar = trim(filter_input(INPUT_POST, 'powerCar'));
        $fuelCar = trim(filter_input(INPUT_POST, 'fuelCar'));
        $colorCar = trim(filter_input(INPUT_POST, 'colorCar'));
        $nbrPlateCar = trim(filter_input(INPUT_POST, 'nbrPlateCar'));
        $nbrKilometersCar = trim(filter_input(INPUT_POST, 'nbrKilometersCar'));
        $tireCar = trim(filter_input(INPUT_POST,'tireCar'));
        $priceCar = trim(filter_input(INPUT_POST,'priceCar'));
        $powersterringCar = filter_input(INPUT_POST, 'powersterringCar');
        $catCar = filter_input(INPUT_POST,'catCar');
        $brandCar = filter_input(INPUT_POST, 'brandCar');
        if(isset($_FILES['imgCar']['name'])){
            $file_name_without_extension =  $controllerCar->before('.',$_FILES['imgCar']['name']);
        }
        $file_name = $file_name_without_extension.'.jpg';
        $file_destination = "../../Public/Thumbs/".$controllerCar->before('.',$_FILES['imgCar']['name']).".jpg";

        if($dataCar != null){
            $dataCar->setName($nameCar);
            $dataCar->setNumberPlace($nbrPlaceCar);
            $dataCar->setPower($powerCar);
            $dataCar->setTypeFuel($fuelCar);
            $dataCar->setColor($colorCar);
            $dataCar->setImmatriculation($nbrPlateCar);
            $dataCar->setCarTire($tireCar);
            $dataCar->setPowerSterring($powersterringCar);
            $dataCar->setNbrKilometres($nbrKilometersCar);
            $dataCar->setPrice($priceCar);
            $dataCar->setIdCat($catCar);
            $dataCar->setIdBrand($brandCar);
            $dataCar->setNameFile($file_name);
            $dataCar->setFile($file_destination);

            return $dataCar;
        }else{

            $carData = new Cars(null,$nameCar, $nbrPlaceCar, $powerCar, $fuelCar, $colorCar, $nbrPlateCar,null, $tireCar, $powersterringCar, $nbrKilometersCar, $priceCar, $catCar, $brandCar, $file_name, $file_destination);
            return $carData;
        }
    }

    /**
     * @param $file_extension
     * @param $extension_authorized
     * @param $file_error_verif
     * @param $file_tmp_name
     * @param $file_destination
     * @return array|bool
     * This function allow to verify if file is valid before of put in BDD
     */
    public function validateFileUpdate($file_extension,$extension_authorized,$file_error_verif,$file_tmp_name,$file_destination,$delete_old_file=null){
        unlink($delete_old_file);
        if(in_array($file_extension,$extension_authorized) && $file_error_verif == 0){

            if(move_uploaded_file($file_tmp_name,$file_destination)){
                return true;
            }else{
                $error[] = "Just a little problem with your file while the transfer";
            }
        }else{
            $error[] = "The file isn't authorized, you must enter (jpeg, png or jpg) thanks";
        }
        return $error;
    }


    /**
     * @param null $dataUpdateCar
     * check data for cars
     * @return array
     */
    public function isValidUpdateDataCar($dataUpdateCar=null, $cat=null, $brand =null){
        $error =[];

        if(empty($dataUpdateCar->getName()) || mb_strlen($dataUpdateCar->getName()) < 5 || mb_strlen($dataUpdateCar->getName()) > 100 || !preg_match("#[a-zA-Z0-9-]#", $dataUpdateCar->getName())){

            $error[] = "The name isn't correct, you must enter a name between 2 and 60 characters";
        }


        if(empty($dataUpdateCar->getNumberPlace())  || $dataUpdateCar->getNumberPlace() < 2 || $dataUpdateCar->getNumberPlace() > 9 || !preg_match("#^[0-9]$#", $dataUpdateCar->getNumberPlace())){

            $error[]= "The number of place isn't correct, you must enter a firstname between 2 and 9 places";
        }

        if($dataUpdateCar->getPower() === "" || mb_strlen($dataUpdateCar->getPower()) < 5 || mb_strlen($dataUpdateCar->getPower()) > 100 || !preg_match("#[A-Za-z0-9-]*#", $dataUpdateCar->getPower())){
            $error[]= "The power isn't correct";
        }

        if($dataUpdateCar->getTypeFuel() === "" || !preg_match("#^[a-zA-Z0-9-]*$#", $dataUpdateCar->getTypeFuel()) || mb_strlen($dataUpdateCar->getTypeFuel()) < 5 || mb_strlen($dataUpdateCar->getTypeFuel()) > 20 ){
            $error[]= "The type of fuel  isn't correct";
        }

        if(empty($dataUpdateCar->getColor() )|| mb_strlen($dataUpdateCar->getColor()) < 3 || mb_strlen($dataUpdateCar->getColor()) > 100 || !preg_match("#[a-zA-Z0-9-\#]#", $dataUpdateCar->getColor())){

            $error[] = "The color isn't correct, you must enter a color between 4 and 100 characters";
        }
        if(empty($dataUpdateCar->getImmatriculation()) ||  mb_strlen($dataUpdateCar->getImmatriculation()) > 9  || !preg_match("#^[A-Z]{2}[-][0-9]{3}[-][A-Z]{2}$#", $dataUpdateCar->getImmatriculation())){

            $error[] = "The number plate isn't correct, you must enter a number plate of 9 characters";
        }

        if($dataUpdateCar->getNbrKilometres() === "" || mb_strlen($dataUpdateCar->getNbrKilometres()) < 0 || mb_strlen($dataUpdateCar->getNbrKilometres()) > 7 || !preg_match("#[0-9 ]*#", $dataUpdateCar->getNbrKilometres())){

            $error[] = "The number of kilometers isn't correct, you must enter a number between 0 and 6 numbers";
        }

        if($dataUpdateCar->getCarTire() === "" || mb_strlen($dataUpdateCar->getCarTire()) < 4 || mb_strlen($dataUpdateCar->getCarTire()) > 20 || !preg_match("#^[a-zA-Z0-9-]*$#", $dataUpdateCar->getCarTire())){

            $error[] = "The tire isn't correct, you must enter a name of tire between 4 and 20 characters";
        }

        if($dataUpdateCar->getPrice() === "" || !preg_match("#^[0-9.\,]*$#", $dataUpdateCar->getPrice())){

            $error[] = "The price isn't correct, you must enter just a number";
        }
        if($dataUpdateCar->isPowerSterring() === ""  || !preg_match("#^[0-1]*$#", $dataUpdateCar->isPowerSterring())){

            $error[] = "The value of power sterring isn't correct, you must enter a value";
        }

        // i must verify if cat is in the BDD
        if ($dataUpdateCar->getIdCat() != "") {

            $category = false;
            $i=1;
            while ($i < count($cat) ) {
                if ($cat[$i]->getId() == $dataUpdateCar->getIdCAt()) {
                    $category = true;
                }
                $i++;
            }
            if ($category == false) {
                $errors[] = "The category isn't in BDD!";
            }
        }else{
            $error[] = "The value of the category isn't correct, you must enter a value";
        }

        // i must verify if brand is in the BDD
        if ($dataUpdateCar->getIdBrand() != "") {

            $brandCar = false;
            $i=0;
            while($i <count($brand)) {
                if ($brand[$i]->getId() == $dataUpdateCar->getIdBrand()) {
                    $brandCar = true;
                }
                $i++;
            }
            if ($brandCar == false) {
                $errors[] = "The brand isn't in BDD!";
            }
        }else{
            $error[] = "The value of the brand isn't correct, you must enter a value";
        }

        return $error;
    }


    /**
     * @param $file
     * function of debug
     */
    public function debug($file){
        echo "<pre>";
        echo var_dump($file);
        echo "</pre>";
        exit();
    }
}