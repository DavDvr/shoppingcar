<?php

namespace App\Controllers;


use App\Dao\DaoBrand;
use App\Dao\DaoCars;
use App\Dao\DaoCategory;
use App\Models\Cars;
use Intervention\Image\ImageManager;
use App\Security\{ValidationDataCars};
use App\Session\{Session, SessionCar};
use App\Dao\{DaoCustomers};
class CarsController
{


    private $catDao;
    private $daoBrand;
    private $daoCar;
    private $error;
    private $session;
    private $car;
    private $manager;
    private $daoCustomers;

    public function __construct($error=[], $car=null)
    {
        $this->catDao = new DaoCategory();
        $this->daoBrand = new DaoBrand();
        $this->daoCar = new DaoCars();
        $this->session = new Session();
        $this->car = $car;
        $this->error=$error;
        $this->manager = new ImageManager(['driver'=>'imagick']);
        $this->daoCustomers = new DaoCustomers();
    }

    public function getAdmin(){
        $admin =$this->daoCustomers->findByRole();
        if($admin->getRole() && $admin->getFirstName() === $_SESSION['auth']){
            return true;
        }
    }


    /**
     * @return array|string
     * This function return all the category
     */
    public function findAllCat(){
        $allCat =$this->catDao->findAll();
        return $allCat;
    }


    /**
     * @return array|false|string
     * This function return all the brand
     */
    public function findAllBrand(){

        $allBrand = $this->daoBrand->findByAllBrand();
        return $allBrand;
    }

    function before ($search, $inthat)
    {
        return substr($inthat, 0, strpos($inthat, $search));
    }

    public function deleteCar($idcar=null){
       if($idcar !== null){
           $carImg = $this->daoCar->findById($idcar);
           $file = $carImg->getFile();
           if(file_exists($file)){
               unlink($file);
           }
           $this->daoCar->deleteCars($idcar);
           $msgDelete = array("You've delete the car with successfully");
           $this->session->setFlash($msgDelete,'deleteSuccess','success');
       }

    }

    /**
     * @param $findAllcat
     * @param $findAllBrand
     * This function make a treatment on the data form of the car and the file
     * @return Cars|mixed
     */
    public function treatment($findAllcat,$findAllBrand){

        if( $this->getAdmin()){

            if(isset($_POST['btnSubmitCar']) && !empty($_POST) && !empty($_FILES)){
                //_____________________________________________________FOR FILE

                //give me the first part of my file before the dot
                $file = $this->before('.',$_FILES['imgCar']['name']);
                //extension
                $file_extension = '.jpg';
                //verif file error 0
                $file_error_verif = $_FILES['imgCar']['error'];
                //file destination take name original file after that i add the extension
                $file_destination = '../../Public/Thumbs/'.$file.$file_extension;
                $extension_authorized = array('.jpeg', '.jpg', '.png','.JPEG', '.JPG', '.PNG');

                //use file tempo to make resize my picture with the same ratio

                $file_tmp_name = $this->manager->make($_FILES['imgCar']['tmp_name'])->resize(300,300,function ($constraint){
                    $constraint->aspectRatio();
                })
                    ->interlace(true)
                    ->save('../../Public/Thumbs/'.$file.$file_extension,100);



                //________________________________________________________VALIDATION DATA
                $validDataCar = new ValidationDataCars();

                $this->car = $validDataCar->recupDataCar();
                $this->error = $validDataCar->isValidDataCar($this->car,$findAllcat, $findAllBrand);
                $isTrue =$validDataCar->validateFile($file_extension,$extension_authorized,$file_error_verif,$file_tmp_name,$file_destination);

                if(count($this->error) == 0 && $isTrue){

                    $this->daoCar->InsertCars($this->car);
                    $success []="Cars insert with success";
                    $this->session->setFlash($success,'success','success');
                }else{
                    $error[] = "Just a little problem with your file or your data, you can verify if the car or file is already in database";
                    $this->session->setFlash($this->error,'danger');
                }

                return $this->car;

            }
        }else{

            header('Location: ../Logged/viewFormConnect.php');
        }

    }

    public function displayLike($idCar){
        return $this->daoCar->selectNbrLike($idCar);
    }

    /**
     * function that allow a user of like and dislike a car
     *
     */
    public function like(){
        if(isset($_SESSION['auth']) && !empty($_SESSION['auth'])){

            if(isset($_GET['type'],$_GET['id']) && !empty($_GET['type']) && !empty($_GET['id'])){

                $getType = filter_input(INPUT_GET,'type');
                $getId =(int)filter_input(INPUT_GET,'id');
                $sessionIdCustomer = $_SESSION['idCustomer'];

                if($this->daoCar->findById($getId)){

                    if ($getType == 1 && !empty($sessionIdCustomer)){

                      if($this->daoCar->checkLike($getId,$sessionIdCustomer)){

                          $this->daoCar->deleteLike($getId,$sessionIdCustomer);
                      }else{
                          $this->daoCar->insertLike($getId,$sessionIdCustomer);
                      }

                    }else{
                        header("Location: ../AccueilAndAllCars/viewAllCar.php");
                    }

                }else{
                    header("Location: ../AccueilAndAllCars/viewAllCar.php");
                }

            }
        }else{
            header("Location: ../Logged/viewFormConnect.php");
        }
    }


}