<?php
namespace App\Controllers;


use App\Dao\{DaoBrand};
use App\Security\{ValidationDataBrand};
use App\Session\{Session};
use App\Controllers\{CarsController};
class BrandController
{

    private $daoBrand;
    private $error = [];
    private $controllerCar;
    public function __construct($error=null)
    {
        $this->daoBrand = new DaoBrand();
        $this->error = $error;
        $this->controllerCar= new CarsController();
    }

    public function treatmentBrand(){

       if($this->controllerCar->getAdmin()){

            $session = new Session();
            if(isset($_POST['btnSubmitBrand']) && !empty($_POST)){

                $validationDataBrand = new ValidationDataBrand();
                $brand = $validationDataBrand->recupDataBrand();
                $this->error = $validationDataBrand->isValidBrandData($brand);

                if(count($this->error) == 0){

                    $this->daoBrand->insertBrand($brand);
                    $success []="Brand insert with success";
                    $session->setFlash($success,'successBrand','success');

                }else{
                    $session->setFlash($this->error,'dangerBrand');

                }
            }

       }else{
           header('Location: ../Logged/viewFormConnect.php');
       }

    }


}