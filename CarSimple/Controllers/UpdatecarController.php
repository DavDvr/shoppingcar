<?php


namespace App\Controllers;

use App\Security\ValidationUpdateCar;
use App\Session\Session;
use App\Session\SessionCar;
use App\Dao\{DaoBrand, DaoCars, DaoCategory};
use Intervention\Image\ImageManager;

class UpdatecarController
{


    private $catDao;
    private $daoBrand;
    private $daoCar;
    private $error;
    private $session;
    private $car;
    private $manager;
    private $validUpdateDataCar;
    private $controllerCar;

    public function __construct($error=[], $car=null)
    {
        $this->catDao = new DaoCategory();
        $this->daoBrand = new DaoBrand();
        $this->daoCar = new DaoCars();
        $this->session = new Session();
        $this->car = $car;
        $this->error=$error;
        $this->validUpdateDataCar = new ValidationUpdateCar();
        $this->manager = new ImageManager(['driver'=>'imagick']);
        $this->controllerCar = new CarsController();
    }

    /**
     * @return array|string
     * This function return all the category
     */
    public function findAllCat(){
        return $this->catDao->findAll();
    }

    public function findNameCatById(): ?string
    {
        $idCar =$this->findById();
       $carNameCat = $this->catDao->findById($idCar['idCat']);
       return $carNameCat->getNameCategory();
    }

    public function findNameBrandById(): ?string
    {
        $idCar = $this->findById();
        $carNameBrand = $this->daoBrand->findById($idCar['idBrand']);
        return $carNameBrand->getName();
    }


    /**
     * @return array|false|string
     * This function return all the brand
     */
    public function findAllBrand(){
        return $this->daoBrand->findByAllBrand();
    }

    function before ($search, $inthat)
    {
        return substr($inthat, 0, strpos($inthat, $search));
    }

    public function findById(){
        if (isset($_GET['updateId']) && !empty($_GET['updateId'])){

            $idCar = (int)trim(filter_input(INPUT_GET,'updateId'));
            return $this->daoCar->findById($idCar);

        }
    }

    /**
     * @param $findAllcat
     * @param $findAllBrand
     * This function make a treatment on the data form of the car and the file
     */
    public function treatmentUpdateCar($findAllcat,$findAllBrand){

    if($this->controllerCar->getAdmin()){

        if(isset($_POST['btnSubmitUpdateCar']) && !empty($_POST) ){

            $this->car= $this->findById();

            //_____________________________________________________FOR FILE

            //give me the first part of my file before the dot for new and old image
            $oldImageFile = ($_POST['oldImgCar']);
            $newFile = $this->before('.',$_FILES['imgCar']['name']);
            $file_extension = '.jpg';

            $file_error_verif = $_FILES['imgCar']['error'];
            //file destination take name original file after that i add the extension
            $file_destination = '../../Public/Thumbs/'.$newFile.$file_extension;
            //old file destination
            $oldFilePath = "../../Public/Thumbs/".$oldImageFile;

            //extension authorized
            $extension_authorized = array('.jpeg', '.jpg', '.png','.JPEG', '.JPG', '.PNG');


            if(file_exists($file_destination)){
                $error  = array("The image must be different of the image already post");
                $this->session->setFlash($error,'danger','info');

            }else{
                //________________________________________________________VALIDATION DATA
                if($_FILES['imgCar']['name'] != ""){

                    //use file tempo to make resize my picture with the same ratio
                    $file_tmp_name = $this->manager->make($_FILES['imgCar']['tmp_name'])->resize(300,300,function ($constraint){
                        $constraint->aspectRatio();
                    })
                        ->interlace(true)
                        ->save('../../Public/Thumbs/'.$newFile.'.jpg',100);


                    $this->validUpdateDataCar->recupDataUpdateCar($this->car);
                    $this->error = $this->validUpdateDataCar->isValidUpdateDataCar($this->car,$findAllcat, $findAllBrand);
                    $isTrue =$this->validUpdateDataCar->validateFileUpdate($file_extension,$extension_authorized,$file_error_verif,$file_tmp_name,$file_destination,$oldFilePath);


                    //__________________________________________________________if not error
                    if(count($this->error) == 0 && $isTrue){
                        $this->daoCar->updateCars($this->car);
                        $success []="The cars is update with success";
                        $this->session->setFlash($success,'success','success');

                    }else{
                        $error[] = "Just a little problem with your file or your data, you can verify if the car or file is already in database";
                        $this->session->setFlash($this->error,'danger');
                    }
                }else{

                    $this->validUpdateDataCar->recupDataUpdateCar($this->car);
                    $this->error = $this->validUpdateDataCar->isValidUpdateDataCar($this->car,$findAllcat, $findAllBrand);

                    //__________________________________________________________if not error
                    if(count($this->error) == 0){

                        $this->daoCar->updateCars($this->car,$oldImageFile,$oldFilePath);
                        $success []="The cars is update with success";
                        $this->session->setFlash($success,'successUpdate','success');

                    }else{
                        $error = array("Just a little problem with your file or your data, you can verify if the car or file is already in database");
                        $this->session->setFlash($error,'danger');
                    }
                }
            }
        }
    }else{
        header('Location: ../Logged/viewFormConnect.php');
    }
}





}