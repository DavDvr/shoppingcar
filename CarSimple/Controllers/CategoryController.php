<?php


namespace App\Controllers;
use App\Security\ValidationDataCategory;
use App\Session\{Session};
use App\Dao\{DaoCategory};

class CategoryController
{
    private $daoCat;
    private $controllerCar;

    public function __construct()
    {
        $this->daoCat = new DaoCategory();
        $this->controllerCar = new CarsController();
    }

    /**
     *This function make a treatment on the data of form for the category
     */
    public function treatmentCategory(){

        if($this->controllerCar->getAdmin()){

            $session = new Session();

            if(isset($_POST['btnSubmitCat']) && !empty($_POST)){

                $validationDataCat = new ValidationDataCategory();

                $cat = $validationDataCat->recupDataCategory();
                $error = $validationDataCat->validDataCategory($cat);

                if(count($error) == 0){

                        $this->daoCat->insertCat($cat);
                        $success[]= "Cat is insert with success";
                        $session->setFlash($success,'successCat','success');

                }else{
                    $session->setFlash($error,'dangerCat');
                }
            }
        }else{
            header('Location: ../Logged/viewFormConnect.php');
        }

    }




}