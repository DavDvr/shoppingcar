<?php


namespace App\Controllers;

require '../../vendor/autoload.php';
use App\Dao\{DaoCars,DaoCategory};
use Intervention\Image\ImageManager;


class AllcarsController
{
    private $carCrtl;
    private $getter;
    private $daoCars;
    private $daoCat;
    private $manager;


    /**
     * AllcarsController constructor.
     */
    public function __construct($getter=null){
        $this->daoCars= new DaoCars();
        $this->carCrtl = new CarsController();
        $this->getter =$getter;
        $this->daoCat= new DaoCategory();
        $this->manager = new ImageManager(['driver'=>'imagick']);
    }



    public function path(){
        $daoCars=new DaoCars();
        $valuecars = $daoCars->findAll();
        foreach($valuecars as $car){
          echo $car->getFile();
        }
    }

    public function resizeImage(){
        $images = glob('../../Public/Uploads/*.jpg');

        //watermark permet de placer une image sur une autre image via insert plus bas
        $watermark = $this->manager
            ->make('../../Public/Icons/icons8-checked-user-male-26.png')
            ->opacity(30);

        foreach($images as $image){
            $this->manager
                ->make($image)
                ->orientate()
                ->resize(200,null,function ($constraint){
                    $constraint->aspectRatio();
                 })
                 ->interlace(true)
                 ->insert($watermark,'bottom-right',10,10)
                 ->text('Salut les gens',10,10,function($font){
                     $font->file('fichier font a importer dans projet');
                     $font->color('#FFFFFF');
                     $font->size(24);
                     $font->valign('top');
                 })
                 ->save('../../Public/Thumbs/'.pathinfo($image,PATHINFO_FILENAME).'.jpg')
                 ->destroy();
        }

        $watermark->destroy();
    }




}