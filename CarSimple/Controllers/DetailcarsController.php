<?php


namespace App\Controllers;



use App\Dao\{DaoCars};

class DetailcarsController
{
    private $daoCars;


    public function __construct(){
        $this->daoCars = new DaoCars();
    }

    public function displayDetailCar(){
        if(isset($_SESSION['auth']) && !empty($_SESSION['auth'])){

            if (isset($_GET['idCar']) && !empty($_GET['idCar'])){

                $id = (int)trim(filter_input(INPUT_GET,'idCar'));

               return  $detailCar =$this->daoCars->findById($id);

            }
        }else{
            header("Location: ../Logged/viewFormConnect.php");
        }
    }
}