<?php


namespace App\Controllers;

use App\Security\{ValidationDataCustomer};
use App\Dao\{DaoCustomers};
use App\Session\{Session};
class AccountController
{
    private $validationDataCustom;
    private $daoCustomer;
    private $session;

    public function __construct()
    {
        $this->validationDataCustom = new ValidationDataCustomer();
        $this->daoCustomer = new DaoCustomers();
        $this->session = new Session();
    }



    public function treatmentAccount(){

        if (isset($_POST['btn-submit']) && !empty($_POST)) {

        $customer = $this->validationDataCustom->recupDataCustomer();
        $error = $this->validationDataCustom->isValidDataInscription($customer);

            if (count($error) == 0){

                  $this->daoCustomer->insert($customer);
                  $success []= "Your account has been create with success";
                  $this->session->setFlash($success,'successAccount','success');
            }else{
                //var_dump($error);
                //exit();
                $this->session->setFlash($error,'dangerAccount');
            }

        }
    }

}