<?php


namespace App\Controllers;


use App\Dao\DaoCustomers;
use App\Security\ValidationDataCustomer;
use App\Session\Session;

class LoggedController
{

    private $session;
    private $validationDataCustom;
    private $customerLogin;
    private $error;
    private $daoCustomer;

    public function __construct($session =null, $customerLogin=null, $error=null)
    {
        $this->session= new Session();
        $this->validationDataCustom = new ValidationDataCustomer();
        $this->customerLogin= $customerLogin;
        $this->error = $error;
        $this->daoCustomer = new DaoCustomers();
    }


    public function isErrorLoggin(){

        $this->customerLogin= $this->validationDataCustom->recupDataForLogin();
        $this->error = $this->validationDataCustom->validDataFormLogin($this->customerLogin);

        return $this->error;
    }


    public function logged(){

        if(isset($_POST['btnSubmit']) && !empty($_POST)){


            if(!$this->isErrorLoggin()){

                //i find the information of customer for my session
                $customer = $this->daoCustomer->findByLogin($this->customerLogin->getEmail());

                $_SESSION['auth'] = ucfirst($customer->getFirstName());
                $_SESSION['idCustomer'] = (int)$customer->getId();

                if(isset($_SESSION['auth'], $_SESSION['idCustomer']) && !empty($_SESSION['auth']) && !empty($_SESSION['idCustomer'])){
                    $success [] = "You are logged successfully";
                    $this->session->setFlash($success,'success','success');

                }else{
                    $error[]="You are not logged";
                    $this->session->setFlash($this->error,'danger');
                }
            }else{
                $this->session->setFlash($this->error,'danger');
            }

        }

    }

}