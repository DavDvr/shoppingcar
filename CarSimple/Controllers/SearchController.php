<?php


namespace App\Controllers;

require '../../vendor/autoload.php';
use App\Security\ValidationSearch;
use App\Dao\{DaoCars};
class SearchController
{

    private $daoCar;
    private $validSearch;

    public function __construct(){

        $this->daoCar= new DaoCars();
        $this->validSearch = new ValidationSearch();
    }

    public function treatment(){

        if (isset($_POST['searchCar']) && !empty($_POST['searchCar']) && isset($_POST['btn-Search'])){

           $dataSearch = $this->validSearch->recupDataSearch();
           $error = $this->validSearch->validationFormSearch($dataSearch);
           if(count($error) == 0){

            return $this->validSearch->searchAllBeginBy($dataSearch);

           }
        }

    }
}