<?php


namespace App\Dao;


interface InterfaceDaoBrand
{
    function insertBrand($brand);
    function findById($id);
    function findByAllBrand();
    function updateBrand($brand);
    function deleteBrand($id);
}