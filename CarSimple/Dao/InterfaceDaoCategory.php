<?php
namespace App\Dao;

interface InterfaceDaoCategory
{
    function insertCat($cat);
    function findById($id);
    function findAll();
    function deleteCat($id);
    function updateCat($cat);

}