<?php


namespace App\Dao;
use App\Connect\Database;
use App\Models\CarBrand;
use PDO;

class DaoBrand implements InterfaceDaoBrand
{

    function insertBrand($brand)
    {
        try {
            $pdo = Database::connect();
            $req = "insert into carbrand (name, description, carmanufacturingplace, controlquality) 
                values(:name, :description, :carmanufacturingplace, :controlquality)";
            $stat = $pdo->prepare($req);
            $executeInsertBrand = $stat->execute([':name'=>$brand->getName(),
                ':description'=>$brand->getDescription(),
                ':carmanufacturingplace'=>$brand->getCarManufacturingPlace(),
                ':controlquality'=>$brand->getControlQuality()]);

            return $executeInsertBrand;

        }catch(\Exception $exc){

            return 'Just a litle problem with the function InsertBrand: '.$exc->getMessage();
        }
    }

    function findById($id)
    {
        $pdo = Database::connect();
        $req = "select * from carbrand where id= :id";
        $statFindBrand = $pdo->prepare($req);

        try{
            $statFindBrand->execute([':id'=>$id]);
            $resultBrand = $statFindBrand->fetch(PDO::FETCH_ASSOC);

            if($resultBrand != null){

                $carBrand = new CarBrand();

                $carBrand->setName($resultBrand['name']);
                $carBrand->setDescription($resultBrand['description']);
                $carBrand->setCarManufacturingPlace($resultBrand['carmanufacturingplace']);
                $carBrand->setControlQuality($resultBrand['controlquality']);

                return $carBrand;
            }
            return false;
        }catch(\Exception $excp){

            return 'Just a little problem with the findById : '. $excp->getMessage();
        }



    }

    function findByAllBrand()
    {
       $pdo = Database::connect();
       $req = "select * from carbrand order by name";
        try
        {
           $statAllBrand = $pdo->query($req);
           $resultAll = $statAllBrand->fetchAll(PDO::FETCH_ASSOC);
           $tabAllBrand =[];

           if($resultAll != null){
            foreach ($resultAll as $brands){

                $carBrandAll = new CarBrand();

                $carBrandAll->setId($brands['id']);
                $carBrandAll->setName($brands['name']);
                $carBrandAll->setDescription($brands['description']);
                $carBrandAll->setCarManufacturingPlace($brands['carmanufacturingplace']);
                $carBrandAll->setControlQuality($brands['controlquality']);

                 $tabAllBrand [] = $carBrandAll;
            }
                return $tabAllBrand;
           }
           return false;

        }catch(\Exception $excep){
            return 'Just a little problem with the findByAll :' . $excep->getMessage();
        }
    }

    function updateBrand($brand)
    {
        $pdo = Database::connect();
        $req = "update from carbrand set :name, :description, :carmanufacturingplace, :controlquality where id= :id";
        try {

            $statUpdateBrand = $pdo->prepare($req);
            $executeUpdateBrand = $statUpdateBrand->execute([':name'=>$brand->getName(),
                                                             ':description'=>$brand->getDescription(),
                                                             ':carmanufacturingplace'=>$brand->getManufacturingPlace(),
                                                             ':controlquality'=>$brand->getControlQuality()]);

            return $executeUpdateBrand;
        }catch(\Exception $ex){

            return 'You have just a little problem with the updateBrand :' . $ex->getMessage();
        }

    }

    function deleteBrand($id)
    {
        $pdo = Database::connect();
        $req = 'delete * from carbrand where id= :id';

        try {

            $statDeleteBrand = $pdo->prepare($req);
            $resultDeleteBrand = $statDeleteBrand->execute([':id'=>$id]);

            return $resultDeleteBrand;

        }catch (\Exception $exept){

            return 'You have just a problem with the function deleteBrand :' .$exept->getMessage();
        }
    }
}