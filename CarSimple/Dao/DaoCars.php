<?php
namespace App\Dao;

use App\Connect\Database;
use App\Models\Cars;
use PDO;

//use App\Dao\InterfaceDaoCars;

class DaoCars implements InterfaceDaoCars
{


    /**
     * @param $id
     * @return array|false|mixed
     */
    function findById($id=null)
    {

        $pdo = Database::connect();
        $req = "select * from CARS where id= :id";
        $stat = $pdo->prepare($req);
        try {
            $stat->execute([':id'=>$id]);
            $tabCar = $stat->fetch(PDO::FETCH_ASSOC);

            if($tabCar != null){

                $car = new Cars();

                $car->setId($tabCar['id']);
                $car->setName($tabCar['name']);
                $car->setNumberPlace($tabCar['numberPlace']);
                $car->setPower($tabCar['power']);
                $car->setTypeFuel($tabCar['typeFuel']);
                $car->setColor($tabCar['color']);
                $car->setImmatriculation($tabCar['immatriculation']);
                $car->setCarTire($tabCar['carTire']);
                $car->setPowerSterring($tabCar['powerSterring']);
                $car->setNbrKilometres($tabCar['nbrKilometres']);
                $car->setPrice($tabCar['price']);
                $car->setIdCat($tabCar['idCat']);
                $car->setIdBrand($tabCar['idBrand']);
                $car->setNameFile($tabCar['nameFile']);
                $car->setFile($tabCar['file']);

                return $car;

            }else{
                return false;
            }
        }catch(\Exception $e){
            return 'just a problem with findById : '. $e->getMessage();
        }

    }

    /**
     * @inheritDoc
     */
    function findByNumberPlate($numberPlate=null)
    {
        $pdo = Database::connect();
        $req = "select * from CARS where immatriculation = :immatriculation";
        $stat = $pdo->prepare($req);
        try {
            $stat->execute([':immatriculation'=>$numberPlate]);
            $tablCarsWithImmatriculation = $stat->fetch(PDO::FETCH_ASSOC);

            if($tablCarsWithImmatriculation !== null){

                $cars = new Cars();

                $cars->setName($tablCarsWithImmatriculation['name']);
                $cars->setNumberPlace($tablCarsWithImmatriculation['numberPlace']);
                $cars->setPower($tablCarsWithImmatriculation['power']);
                $cars->setTypeFuel($tablCarsWithImmatriculation['typeFuel']);
                $cars->setColor($tablCarsWithImmatriculation['color']);
                $cars->setImmatriculation($tablCarsWithImmatriculation['immatriculation']);
                $cars->setEntryDate($tablCarsWithImmatriculation['entryDate']);
                $cars->setCarTire($tablCarsWithImmatriculation['carTire']);
                $cars->setPowerSterring($tablCarsWithImmatriculation['powerSterring']);
                $cars->setPrice($tablCarsWithImmatriculation['price']);
                $cars->setNameFile($tablCarsWithImmatriculation['nameFile']);
                $cars->setFile($tablCarsWithImmatriculation['file']);

                return $cars;
            }

            return false;
        }catch(\Exception $e){
            return 'just a problem with findByLogin : '. $e->getMessage();
        }

    }

    /**
     * @inheritDoc
     * This function return All the cars
     */
    function findAll()
    {
        try {

            $pdo = Database::connect();
            $req = "select * from CARS ORDER BY entryDate DESC";
            $stat = $pdo->query($req);
            $tabCars = $stat->fetchAll(PDO::FETCH_ASSOC);

            $tablAllCars = [];

            foreach($tabCars  as $tabCar){
                $allCars = new Cars();
                $allCars->setId($tabCar['id']);
                $allCars->setName($tabCar['name']);
                $allCars->setNumberPlace($tabCar['numberPlace']);
                $allCars->setPower($tabCar['power']);
                $allCars->setTypeFuel($tabCar['typeFuel']);
                $allCars->setColor($tabCar['color']);
                $allCars->setImmatriculation($tabCar['immatriculation']);
                $allCars->setCarTire($tabCar['carTire']);
                $allCars->setPowerSterring($tabCar['powerSterring']);
                $allCars->setNbrKilometres($tabCar['nbrKilometres']);
                $allCars->setPrice($tabCar['price']);
                $allCars->setIdCat($tabCar['idCat']);
                $allCars->setIdBrand($tabCar['idBrand']);
                $allCars->setNameFile($tabCar['nameFile']);
                $allCars->setFile($tabCar['file']);

                $tablAllCars[] = $allCars;
            }

            //i return this tab
            return $tablAllCars;

        }catch (\Exception $excep){

            return 'Just a litle problem with the function findAll: '.$excep->getMessage();
        }

    }




    /**
     * @inheritDoc
     * This function insert a car
     */
    function InsertCars($cars)
    {
        try {
            $pdo = Database::connect();
            $req = "insert into CARS (name, numberPlace, power, typeFuel, color, immatriculation, entryDate, carTire, powerSterring, nbrKilometres, price, idCat, idBrand, nameFile, file) 
                values(:name, :numberPlace, :power, :typeFuel, :color, :immatriculation, :entryDate, :carTire, :powerSterring, :nbrKilometres, :price, :idCat, :idBrand, :nameFile, :file)";
            $stat = $pdo->prepare($req);
            $executeInsert = $stat->execute([

                ':name'=>$cars->getName(),
                ':numberPlace'=>$cars->getNumberPlace(),
                ':power'=>$cars->getPower(),
                ':typeFuel'=>$cars->getTypeFuel(),
                ':color'=>$cars->getColor(),
                ':immatriculation'=>$cars->getImmatriculation(),
                ':entryDate'=>date_format($cars->getEntryDate(),'Y-m-d H:i:s'),
                ':carTire'=>$cars->getCarTire(),
                ':powerSterring'=>$cars->isPowerSterring(),
                ':nbrKilometres'=>$cars->getNbrKilometres(),
                ':price'=>$cars->getPrice(),
                ':idCat'=>$cars->getIdCat(),
                ':idBrand'=>$cars->getIdBrand(),
                ':nameFile'=>$cars->getNameFile(),
                ':file'=>$cars->getFile()]);

            return $executeInsert;

        }catch(\Exception $exc){

            return 'Just a litle problem with the function InsertCars: '.$exc->getMessage();
        }

    }

    /**
     * @inheritDoc
     */
    function updateCars($cars,$oldImageFile=null,$oldFilePath=null)
    {
        try {

            $pdo = Database::connect();
            $req = "update CARS set name= :name, numberPlace= :numberPlace, power= :power, typeFuel= :typeFuel, color= :color, immatriculation= :immatriculation, entryDate= :entryDate, carTire= :carTire, powerSterring= :powerSterring, nbrKilometres= :nbrKilometres, price= :price, idCat= :idCat, idBrand= :idBrand, nameFile= :nameFile, file= :file  where id= :id";
            $stat = $pdo->prepare($req);
            $executeUpdate = $stat->execute([

                ':name'=>$cars->getName(),
                ':numberPlace'=>$cars->getNumberPlace(),
                ':power'=>$cars->getPower(),
                ':typeFuel'=>$cars->getTypeFuel(),
                ':color'=>$cars->getColor(),
                ':immatriculation'=>$cars->getImmatriculation(),
                ':entryDate'=>date_format($cars->getEntryDate(),'Y-m-d H:i:s'),
                ':carTire'=>$cars->getCarTire(),
                ':powerSterring'=>$cars->isPowerSterring(),
                ':nbrKilometres'=>$cars->getNbrKilometres(),
                ':price'=>$cars->getPrice(),
                ':idCat'=>$cars->getIdCat(),
                ':idBrand'=>$cars->getIdBrand(),
                ':nameFile'=>($cars->getNameFile() !== ".jpg")? $cars->getNameFile() : $oldImageFile,
                'file'=>($cars->getFile() !== "../../Public/Thumbs/.jpg") ? $cars->getFile() : $oldFilePath ,
                ':id'=>$cars->getId()
            ]);

            return $executeUpdate;

        }catch(\Exception $ex){

            return 'Just a litle problem with the function UpdateCars:' .$ex->getMessage();
        }
    }



    /**
     * @inheritDoc
     */
    function deleteCars($id)
    {
        try {
            $pdo = Database::connect();
            $req= "delete from CARS where id= :id";
            $stat = $pdo->prepare($req);
            $stat->bindParam(':id',$id);
            $resultDelete = $stat->execute();

            return $resultDelete;
        }catch (\Exception $e){

            return 'Just a little problem with the function deleteCars: '.$e->getMessage();
        }

    }


    function insertLike($idCars, $idCustomer)
    {
        try{
            $pdo = Database::connect();
            $req = "insert into likes(id_article, id_customer) values(?,?)";
            $state= $pdo->prepare($req);
            $executeInsert = $state->execute([$idCars,$idCustomer]);
            if(!empty($executeInsert)){

                return $executeInsert;
            }else{
                return false;
            }

        }catch (\Exception $excep){

            return "Just a little problem with function insertLike".$excep->getMessage();
        }

    }

    function selectNbrLike($idCars)
    {
        $pdo= Database::connect();
        $req = "select id from likes where id_article= ?";
        $stat = $pdo->prepare($req);
        $stat->execute([$idCars]);
        $nbrLikes = $stat->rowCount();

        if(!empty($nbrLikes)){
            return $nbrLikes;
        }else{
            return false;
        }
    }

    function checkLike($idCars, $idCustomer){
        $pdo=Database::connect();
        $req= "select id from likes where id_article= ? and id_customer= ?";
        $stat= $pdo->prepare($req);
        $stat->execute([$idCars,$idCustomer]);
        $nbrDisLikes = $stat->rowCount();
        if(!empty($nbrDisLikes) && $nbrDisLikes == 1){
            return $nbrDisLikes;
        }
    }

    public function deleteLike($idCars, $idCustomer){
        $pdo= Database::connect();
        $req = "delete from likes where id_article= ? and id_customer= ?";
        $stat = $pdo->prepare($req);
        $stat->execute([$idCars,$idCustomer]);
    }
}