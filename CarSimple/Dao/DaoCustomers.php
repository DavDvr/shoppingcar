<?php
namespace App\Dao;
use App\Connect\Database;
use App\Models\Customers;
use http\Exception;
use PDO;

class DaoCustomers  implements InterfaceDaoCustomer
{

    function insert($customer)
    {
        try {

            $pdo = Database::connect();
            $req = "insert into customers (lastName, firstName, birthdate, address, licenceNumber, passwordCustomer, email) values(:lastName, :firstName, :birthdate, :address, :licenceNumber, :passwordCustomer, :email)";
            $stat = $pdo->prepare($req);
            $executeInsert = $stat->execute([
                ':lastName'=>$customer->getLastName(),
                ':firstName'=>$customer->getFirstName(),
                ':birthdate'=>$customer->getBirthdate(),
                ':address'=>$customer->getAddress(),
                ':licenceNumber'=>$customer->getLicenceNumber(),
                ':passwordCustomer'=>$customer->getPassword(),
                ':email'=>$customer->getEmail()]);

            return $executeInsert;

        }catch(Exception $excep){

            return 'Just a little problem with insert function: '.$excep->getMessage();
        }

    }

    function findById($id)
    {
        $pdo = Database::connect();
        $req = "select * from customers where id= :id";
        $stat= $pdo->prepare($req);

        $stat->execute([':id'=>$id]);
        $tabCustomers = $stat->fetch(PDO::FETCH_ASSOC);

        if($tabCustomers !== null){
            $custom = new Customers();

            $custom->setLastName($tabCustomers['lastName']);
            $custom->setFirstName($tabCustomers['firstName']);
            $custom->setBirthdate($tabCustomers['birthdate']);
            $custom->setAddress($tabCustomers['address']);
            $custom->setLicenceNumber($tabCustomers['licenceNumber']);
            $custom->setPassword($tabCustomers['passwordCustomer']);
            $custom->setEmail($tabCustomers['email']);

            return $custom;
        }
        return false;
    }

    function findByRole()
    {
        $pdo = Database::connect();
        $req = "SELECT * FROM `customers` WHERE `role` IN ('[\"ROLE_ADMIN\"]')";
        $stat= $pdo->prepare($req);

        $stat->execute();
        $tabAdmin = $stat->fetch(PDO::FETCH_ASSOC);

        if($tabAdmin != null){
            $custom = new Customers();

            $custom->setLastName($tabAdmin['lastName']);
            $custom->setFirstName($tabAdmin['firstName']);
            $custom->setBirthdate($tabAdmin['birthdate']);
            $custom->setAddress($tabAdmin['address']);
            $custom->setLicenceNumber($tabAdmin['licenceNumber']);
            $custom->setPassword($tabAdmin['passwordCustomer']);
            $custom->setEmail($tabAdmin['email']);
            $custom->setRole((array)$tabAdmin['role']);

            return $custom;
        }
        return false;

    }

    function findAll()
    {
        // TODO: Implement findAll() method.
    }

    function findByLogin($login)
    {
        $pdo = Database::connect();
        $req = "select * from customers where email= :email";
        $stats= $pdo->prepare($req);

        $stats->execute([':email'=>$login]);
        $tabCustomers = $stats->fetch(PDO::FETCH_ASSOC);

        if($tabCustomers != null){
            $custom = new Customers();
            $custom->setId($tabCustomers['id']);
            $custom->setEmail($tabCustomers['email']);
            $custom->setFirstName($tabCustomers['firstName']);
            $custom->setLastName($tabCustomers['lastName']);
            $custom->setBirthdate($tabCustomers['birthdate']);
            $custom->setAddress($tabCustomers['address']);
            $custom->setLicenceNumber($tabCustomers['licenceNumber']);
            $custom->setRole((array)$tabCustomers['role']);

            return $custom;
        }
        return false;
    }

    function findByEmailAndPassword($email, $password): bool
    {
        $pdo = Database::connect();
        $req = "select * from customers where email= :email";
        $stats= $pdo->prepare($req);

        $stats->execute([':email'=>$email]);
        $tabCustomers = $stats->fetch(PDO::FETCH_OBJ);

        if($tabCustomers != null){
            if(password_verify($password, $tabCustomers->passwordCustomer)){
                return true;
            }else{
                return false;
            }
        }
        return false;
    }

    function updateCustomer($customer)
    {
        try {

            $pdo = Database::connect();
            $req = "update customers set lastName= :lastName, firstName= :firstName, 
                birthdate= :birthdate, address= :address, licenceNumber= :licenceNumber, 
                     passwordCustomer= :passwordCustomer, email= :email";
            $stat = $pdo->prepare($req);
            $executeUpdate = $stat->execute([':lastName'=>$customer->getLastName(),
                ':firstName'=>$customer->getFirstName(),
                ':birthdate'=>$customer->getBirthdate(),
                ':address'=>$customer->getAddress(),
                ':licenceNumber'=>$customer->getLicenceNumber(),
                ':passwordCustomer'=>$customer->getPassword(),
                ':email'=>$customer->getEmail()]);

            return $executeUpdate;

        }catch(Exception $excp){

            return 'Just a litle problem with update function :' .$excp->getMessage();
        }


    }

    function deleteCustomer($id): bool
    {
        $pdo = Database::connect();
        $req = "delete * from customer where id= :id";
        $stat = $pdo->prepare($req);
        $executeDelete = $stat->execute([':id'=>$id]);

        return $executeDelete;
    }
}