<?php
namespace App\Dao;

use App\Connect\Database;
use App\Models\Category;

use PDO;

class DaoCategory implements InterfaceDaoCategory
{

    function insertCat($cat)
    {
        try{
            $pdo = Database::connect();
            $req = "insert into category (name, description) values (:name, :description)";
            $stat= $pdo->prepare($req);
            $executeInsert = $stat->execute([':name'=>$cat->getNameCategory(), ':description'=>$cat->getDescription()]);
            return $executeInsert;
        }catch (\Exception $ex){

            return 'Just a litle problem with the function InsertCat : '.$ex->getMessage();
        }

    }

    function findById($id)
    {
        $pdo = Database::connect();
        $req = "select * from category where id= :id";
        $statCat = $pdo->prepare($req);
        try {
            $statCat->execute([':id'=>$id]);
            $tablCat = $statCat->fetch(PDO::FETCH_ASSOC);

            if($tablCat !== null){

                $cat = new Category();

                $cat->setNameCategory($tablCat['name']);
                $cat->setDescription($tablCat['description']);

                return $cat;
            }

            return false;

        }catch(\Exception $e){
            return 'just a problem with findById for category : '. $e->getMessage();
        }
    }

    function findAll()
    {
        try {

            $pdo = Database::connect();
            $req = "select * from category order by name";
            $statAllCat = $pdo->query($req);
            $resultAllCat = $statAllCat->fetchAll(PDO::FETCH_ASSOC);

            $tabCat[]= '';

            foreach($resultAllCat as $resultAllCats){
                $cats = new Category();

                $cats->setId($resultAllCats['id']);
                $cats->setNameCategory($resultAllCats['name']);
                $cats->setDescription($resultAllCats['description']);

                $tabCat[]= $cats;
            }

            return $tabCat;

        }catch(\Exception $ex){

            return 'just a little problem with function findAll : '.$ex->getMessage();
        }

    }


    function deleteCat($id)
    {
        try{
            $pdo = Database::connect();
            $req = "delete * from category where id= :id";
            $statDelCat= $pdo->prepare($req);
            $resultDelCat= $statDelCat->execute([':id'=>$id]);

            return $resultDelCat;

        }catch(\Exception $exc){

            return 'just a little problem with function deleteCat : '.$exc->getMessage();
        }

    }

    function updateCat($cat)
    {

        try{
            $pdo = Database::connect();
            $req = "update category set name, description where id= :id";
            $stat= $pdo->prepare($req);
            $executeUpdate = $stat->execute([':name'=>$cat->getNameCategory(), ':description'=>$cat->getDescription()]);

            return $executeUpdate;
        }catch (\Exception $ex){

            return 'Just a little problem with the function InsertCat : '.$ex->getMessage();
        }

    }
}