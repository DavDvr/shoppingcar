<?php
namespace App\Dao;

interface InterfaceDaoCars
{
    /**
     * @param $id
     * @return mixed
     * return the user with id
     */
     function findById($id);


    /**
     * @param $numberPlate
     * @return mixed
     * return the car with the number plate
     */
     function findByNumberPlate($numberPlate);

    /**
     * @return mixed
     * Return a array of cars
     */
     function findAll();

    /**
     * @param $cars
     * @return mixed
     * store the new user and assign a unique auto-generated ID
     */
     function InsertCars($cars);

    /**
     * @param $cars
     * @return mixed
     * update a car
     */
     function updateCars($cars);

    /**
     * @param $id
     * @return mixed
     * delete a car with the id
     */
     function deleteCars($id);

    /**
     * @param $id
     * @return mixed
     */
     function insertLike($idCars, $idCustomer);

    /**
     * @param $idCars
     * @return mixed
     */
     function selectNbrLike($idCars);

}