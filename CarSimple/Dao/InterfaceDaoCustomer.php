<?php

namespace App\Dao;

interface InterfaceDaoCustomer
{

    /**
     * @param $customer
     * @return mixed
     * insert a new customer with the id
     */
    function insert($customer);

    /**
     * @param $id
     * @return mixed
     * find customer with her id
     */
    function findById($id);

    /**
     * @return mixed
     * find all customer
     */
    function findAll();

    /**
     * @param $login
     * @return mixed
     * find the customer with her login
     */
    function findByLogin($login);

    /**
     * @param $customer
     * @return mixed
     * update the customer with the new customer
     */
    function updateCustomer($customer);

    /**
     * @param $customer
     * @return mixed
     * delete the customer with her id
     */
    function deleteCustomer($id);
}