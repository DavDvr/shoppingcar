<?php
namespace App\Models;

class CarBrand
{
    private $id;
    private $name;
    private $description;
    private $carManufacturingPlace;
    private $controlQuality;


    public function __construct($id=null, $name=null, $description=null, $carManufacturingPlace=null, $controlQuality=null ){
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
        $this->carManufacturingPlace = $carManufacturingPlace;
        $this->controlQuality = $controlQuality;
    }

    /**
     * @return mixed|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed|null $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }


    /**
     * @return mixed|string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param mixed|string|null $name
     */
    public function setName(?string $name): void
    {
        if(is_string($name)){

            $this->name = $name;
        }

    }

    /**
     * @return mixed|string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param mixed|string|null $description
     */
    public function setDescription(?string $description): void
    {
        if(is_string($description)){

            $this->description = $description;
        }

    }

    /**
     * @return mixed|string|null
     */
    public function getCarManufacturingPlace(): ?string
    {
        return $this->carManufacturingPlace;
    }

    /**
     * @param mixed|string|null $carManufacturingPlace
     */
    public function setCarManufacturingPlace(?string $carManufacturingPlace): void
    {
        if(is_string($carManufacturingPlace)){

            $this->carManufacturingPlace = $carManufacturingPlace;
        }
    }

    /**
     * @return bool|mixed|null
     */
    public function getControlQuality(): ?bool
    {
        return (bool)$this->controlQuality;
    }

    /**
     * @param bool|mixed|null $controlQuality
     */
    public function setControlQuality(?bool $controlQuality): void
    {
        if(is_bool($controlQuality)){
            $this->controlQuality = $controlQuality;
        }
    }



}