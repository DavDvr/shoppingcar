<?php
namespace App\Models;

class Customers
{
    private $id;
    private $lastName;
    private $firstName;
    private $birthdate;
    private $address;
    private $licenceNumber;
    private $password;
    private $email;
    private $role;


    public function __construct($id=null, $lastName=null, $firstName=null, $birthdate=null, $address=null, $licenceNumber=null, $password=null, $email=null, $role=['ROLE_USER']){
        $this->id=$id;
        $this->lastName = $lastName;
        $this->firstName = $firstName;
        $this->birthdate = $birthdate;
        $this->address = $address;
        $this->licenceNumber = $licenceNumber;
        $this->password = $password;
        $this->email = $email;
        $this->role = $role;
    }

    /**
     * @return mixed|string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param mixed|string|null $fistrName
     */
    public function setFirstName(?string $firstName): void
    {
        if(is_string($firstName)){
            $this->firstName = $firstName;
        }
    }

    /**
     * @return mixed|string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param mixed|string|null $lastName
     */
    public function setLastName(?string $lastName): void
    {
        if(is_string($lastName)){
            $this->lastName = $lastName;
        }
    }

    /**
     * @return mixed|string|null
     */
    public function getBirthdate(): ?string
    {
        return $this->birthdate;
    }

    /**
     * @param mixed|string|null $birthdate
     */
    public function setBirthdate(?string $birthdate): void
    {
        if(is_string($birthdate)){
            $this->birthdate = $birthdate;
        }
    }

    /**
     * @return mixed|string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param mixed|string|null $address
     */
    public function setAddress(?string $address): void
    {
        if(is_string($address)){
            $this->address = $address;
        }
    }

    /**
     * @return mixed|string|null
     */
    public function getLicenceNumber(): ?string
    {
        return $this->licenceNumber;
    }

    /**
     * @param mixed|string|null $licenceNumber
     */
    public function setLicenceNumber(?string $licenceNumber): void
    {
        if(is_string($licenceNumber)){
            $this->licenceNumber = $licenceNumber;
        }
    }

    /**
     * @return mixed|string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param mixed|string|null $password
     */
    public function setPassword(?string $password): void
    {
        if(is_string($password)){
            $this->password = $password;
        }
    }

    /**
     * @return int|mixed|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }


    /**
     * @return mixed|string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param mixed|string|null $email
     */
    public function setEmail(?string $email): void
    {
        if(is_string($email)){
            $this->email = $email;
        }
    }

    /**
     * @return mixed|string[]
     */
    public function getRole(): array
    {
        return $this->role;
    }

    /**
     * @param mixed|string[] $role
     */
    public function setRole(array $role): void
    {
        $this->role = $role;
    }





}