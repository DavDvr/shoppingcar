<?php
namespace App\Models;


use DateInterval;
use DateTimeInterface;
use DateTimeZone;

class Cars{

    private $id;
    private $name;
    private $numberPlace;
    private $power;
    private $typeFuel;
    private $color;
    private $immatriculation;
    private $entryDate;
    private $carTire;
    private $powerSterring;
    private $nbrKilometres;
    private $price;
    private $idCat;
    private $idBrand;
    private $nameFile;
    private $file;



    public function __construct($id=null, $name=null, $numberPlace=null, $power=null, $typeFuel=null,$color=null,$immatriculation=null ,$entryDate=null,
                                $carTire=null,$powerSterring=null,$nbrKilometres=null,$price=null,$idCat=null,$idBrand=null,$nameFile=null,$file=null){
        $this->id= $id;
        $this->name = $name;
        $this->numberPlace = $numberPlace;
        $this->power = $power;
        $this->typeFuel = $typeFuel;
        $this->color = $color;
        $this->immatriculation = $immatriculation;
        $this->entryDate = $entryDate;
        $this->carTire = $carTire;
        $this->powerSterring = $powerSterring;
        $this->nbrKilometres=$nbrKilometres;
        $this->price=$price;
        $this->idCat=$idCat;
        $this->idBrand =$idBrand;
        $this->nameFile=$nameFile;
        $this->file=$file;

    }

    /**
     * @return mixed|string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param mixed|string|null $name
     */
    public function setName(?string $name): void
    {
        if(is_string($name)){
            $this->name = $name;
        }
    }

    /**
     * @return int|null
     */
    public function getNumberPlace(): ?int
    {

        return (int)$this->numberPlace;
    }

    /**
     * @param int|null $numberPlace
     */
    public function setNumberPlace(?int $numberPlace): void
    {
        if(is_int($numberPlace)){
            $this->numberPlace = $numberPlace;
        }
    }

    /**
     * @return mixed|string|null
     */
    public function getPower(): ?string
    {
        return $this->power;
    }

    /**
     * @param mixed|string|null $power
     */
    public function setPower(?string $power): void
    {
        if(is_string($power)){
            $this->power = $power;
        }
    }

    /**
     * @return mixed|string|null
     */
    public function getTypeFuel(): ?string
    {
        return $this->typeFuel;
    }

    /**
     * @param mixed|string|null $typeFuel
     */
    public function setTypeFuel(?string $typeFuel): void
    {
        if(is_string($typeFuel)){
            $this->typeFuel = $typeFuel;
        }
    }

    /**
     * @return mixed|string|null
     */
    public function getColor(): ?string
    {
        return $this->color;
    }

    /**
     * @param mixed|string|null $color
     */
    public function setColor(?string $color): void
    {
        if(is_string($color)){
            $this->color = $color;
        }
    }

    /**
     * @return mixed|string|null
     */
    public function getImmatriculation(): ?string
    {
        return $this->immatriculation;
    }

    /**
     * @param mixed|string|null $immatriculation
     */
    public function setImmatriculation(?string $immatriculation): void
    {
        if(is_string($immatriculation)){
            $this->immatriculation = $immatriculation;
        }
    }

    /**
     * @return DateTimeInterface|null
     * @throws \Exception
     */
    public function getEntryDate(): ?DateTimeInterface
    {
        return new \DateTime('now', (new \DateTimeZone('Europe/Paris')));
    }

    /**
     * @param DateTimeInterface $entryDate
     * @return DateTimeInterface
     */
    public function setEntryDate(DateTimeInterface $entryDate): DateTimeInterface
    {
       return $this->entryDate = $entryDate;

    }


    /**
     * @return mixed|string|null
     */
    public function getCarTire(): ?string
    {
        return $this->carTire;
    }

    /**
     * @param mixed|string|null $carTire
     */
    public function setCarTire(?string $carTire): void
    {
        if(is_string($carTire)){
            $this->carTire = $carTire;
        }
    }

    /**
     * @return bool
     */
    public function isPowerSterring(): bool
    {
        return (bool)$this->powerSterring;
    }

    /**
     * @param bool $powerSterring
     */
    public function setPowerSterring(bool $powerSterring): void
    {
        if(is_bool($powerSterring)){
            $this->powerSterring = $powerSterring;
        }
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int|null
     */
    public function getIdCat(): ?int
    {
        return $this->idCat;
    }


    /**
     * @return int|null
     */
    public function getIdBrand(): ?int
    {
        return $this->idBrand;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @param int|null $idCat
     */
    public function setIdCat(?int $idCat): void
    {
        $this->idCat = $idCat;
    }

    /**
     * @param int|null $idBrand
     */
    public function setIdBrand(?int $idBrand): void
    {
        $this->idBrand = $idBrand;
    }

    /**
     * @param string|null $nbrKilometres
     */
    public function setNbrKilometres(?string $nbrKilometres): void
    {
        $this->nbrKilometres = $nbrKilometres;
    }



    /**
     * @return float
     */
    public function getPrice(): float
    {
        return (float)$this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    /**
     * @return string|null
     */
    public function getNbrKilometres(): ?string
    {
        return $this->nbrKilometres;
    }

    /**
     * @param string|null $nbrKilometres
     */
    public function setNbrKilometre(?string $nbrKilometres): void
    {
        $this->nbrKilometres = $nbrKilometres;
    }

    /**
     * @return mixed
     */
    public function getNameFile(): ?string
    {
        return $this->nameFile;
    }

    /**
     * @param mixed $nameFile
     */
    public function setNameFile(?string $nameFile): void
    {
        $this->nameFile = $nameFile;
    }

    /**
     * @return string
     */
    public function getFile(): ?string
    {
        return $this->file;
    }

    /**
     * @param string|null $file
     * @return void
     */
    public function setFile(?string $file): void
    {
        $this->file = $file;
    }




}