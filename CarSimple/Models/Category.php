<?php
namespace App\Models;

class Category
{

    private $id;
    private $nameCategory;
    private $description;

    public function __construct($id= null, $nameCategory = null, $description= null){
        $this->id= $id;
        $this->nameCategory = $nameCategory;
        $this->description= $description;
    }


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed|string|null
     */
    public function getNameCategory(): ?string
    {
        return $this->nameCategory;
    }

    /**
     * @param mixed|string|null $nameCategory
     */
    public function setNameCategory(?string $nameCategory): void
    {
        if(is_string($nameCategory)){
            $this->nameCategory = $nameCategory;
        }
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription(?string $description): void
    {
        if(is_string($description)){
            $this->description = $description;
        }
    }


}