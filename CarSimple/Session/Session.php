<?php


namespace App\Session;

class Session
{

    private $namesession;
    private $namesessionAccess;

    public function __construct($namesession=null, $namesessionAccess=null)
    {
        if (session_status() === PHP_SESSION_NONE) {
            session_start();
        }
        $this->namesession = $namesession;
        $this->namesessionAccess = $namesessionAccess;
    }


    /**
     * @param array $message
     * pass me the session for this message flash
     * @param $namesession
     * @param string $type
     */
    public function setFlash(array $message, $namesession, $type='danger'){
        $_SESSION[$this->namesession] = array('message'=>$message,'type'=>$type);
    }

    public function flashSuccess(){
        if(isset($_SESSION[$this->namesession])){
            ?>
            <div class="alert alert-<?php echo $_SESSION[$this->namesession]['type'];?> alert-dismissible fade show mt-5" role="alert">
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                <?php
                $arraySession = $_SESSION[$this->namesession]['message'];
                if(isset($arraySession) && !empty($arraySession)){
                    foreach($arraySession as $key){
                        echo "$key.<br>";
                    }
                }
                ?>
            </div>
            <?php
            unset($_SESSION[$this->namesession]);
        }
    }

    public function flashDanger(){
        if(isset($_SESSION[$this->namesession])){
            ?>
            <div class="alert alert-<?php echo $_SESSION[$this->namesession]['type'];?> alert-dismissible fade show mt-5" role="alert">
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close" ></button>
                <?php
                $arraySession = $_SESSION[$this->namesession]['message'];
                if(isset($arraySession) && !empty($arraySession)){
                    foreach($arraySession as $key){
                        echo "$key.<br>";
                    }
                }
                ?>
            </div>
            <?php
            unset($_SESSION[$this->namesession]);
        }
    }


    //____________________________________________________________________________________ACCOUNT
    public function flashAccountSuccess(){
        if(isset($_SESSION[$this->namesession])){
            ?>
            <div class="alert alert-<?php echo $_SESSION[$this->namesession]['type'];?> alert-dismissible fade show mt-5" role="alert">
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close" ></button>
                <?php
                $arraySessionSuccess = $_SESSION[$this->namesession]['message'];

                if(isset($arraySessionSuccess) && !empty($arraySessionSuccess)) {
                         foreach ($arraySessionSuccess as $value){
                             echo "$value";
                         }
                     }
                }
                ?>
            </div>
            <?php
            unset($_SESSION[$this->namesession]);
        }


        public function flashAccountDanger(){
        if(isset($_SESSION[$this->namesession])){
            ?>
            <div class="alert alert-<?php echo $_SESSION[$this->namesession]['type'];?> alert-dismissible fade show mt-5" role="alert">
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close" ></button>
                <?php
                $arraySessionDanger = $_SESSION[$this->namesession]['message'];
                if(isset($arraySessionDanger) && !empty($arraySessionDanger)) {
                         foreach ($arraySessionDanger as $value){
                             echo "$value.<br>";
                         }
                     }
                }
                ?>
            </div>
            <?php
            unset($_SESSION[$this->namesession]);
        }

}
