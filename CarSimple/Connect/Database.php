<?php
namespace App\Connect;

use PDO;
use PDOException;

class Database
{
    private static string  $dbName = 'gestCar';
    private static string $dbHost = 'localhost';
    private static string $dbUsername = 'root';
    private static string $dbPassword = '';
    private static  $cont = null;

    public function __construct()
    {
        die('Fonction Init non autorisée');
    }

    /**
     * initailisé juste une fois
     * @return PDO|null
     */
    public static function connect(){
        if(null == self::$cont){
            try {
                self::$cont = new PDO("mysql:host =".self::$dbHost.";"."dbname=".self::$dbName,self::$dbUsername,self::$dbPassword);
                self::$cont->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
            }catch (PDOException $exception){
                die('Un problème s\'est produit lors de la tentative de connexion à la bd :' . $exception->getMessage());
            }
        }

        return self::$cont;
    }

    public static function disconnect(){
        self::$cont = null;
    }



}