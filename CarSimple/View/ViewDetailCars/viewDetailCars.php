<?php
ini_set('memory_limit', '1024M');
//ini_set('display_errors', 1);
require '../../vendor/autoload.php';

use App\Dao\{DaoCategory};
use App\Controllers\{DetailcarsController, SearchController,CarsController};
use App\Session\{Session};
$session = new Session();

$daoCat = new DaoCategory();
$contrlDetailCar = new DetailcarsController();
$carDetail= $contrlDetailCar->displayDetailCar();
//acces controller search bar for display the search of cars
$searchContoler = new SearchController();
$dataSearch = $searchContoler->treatment();
$controllerCar = new CarsController();
$admin =$controllerCar->getAdmin();


?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../Public/StyleMinify.css">
    <?php include '../../Public/bootstrap.php' ?>
    <title>Accueil</title>
</head>
<body class="idColor overflow">
<?php include '../HeaderAndFooter/viewHeader.php' ?>
<?php
if (isset($carDetail) && !empty($carDetail) && empty($dataSearch)){
?>

    <div class='row mx-auto'>
        <div class='col-md-2'>
            <a href='../AccueilAndAllCars/viewAllCar.php'><button type='button' class='btn btn-dark goBack'>Go Back</button></a>
        </div>
        <div class='col-md-8 mt-5 mb-4'>
            <h1 class='h2 text-white text-center titleDetail'><?php echo $carDetail->getName(); ?></h1>
        </div>
    </div>

    <div class='row rowForAll'>

        <div class='col-md-6 mt-2 mb-5 '>
            <p class='text-white priceDetail mb-4'>Price : <?php  echo htmlspecialchars($carDetail->getPrice());?> $</p>
            <p class='textCall'>You want more of details click here </p><img class='imgClickHere' src='../../Public/Icons/icons8-natural-user-interface-2-50.png' alt="icon call us">";
            <img class='w-auto imageEffect rounded imgDetail' src='<?php  echo htmlspecialchars($carDetail->getFile());  ?>'>
        <div class="callUs"> <a href='tel:0640460057'><img title='Call us'  class='iconCallUs' src='../../Public/Icons/icons8-iphone-30.png' alt="icon iphone"></a></div>";

            <?php if(isset($admin)){?>
                <div class='btnUpAndDel'>";
                    <a href='../ViewUpdateCar/viewUpdateCar.php?updateId=<?php  echo htmlspecialchars($carDetail->getId()); ?>'><button type='button' class='btn btn-sm btnUpdate'>Update</button></a>
                    <a href='../AccueilAndAllCars/viewAllCar.php?deleteId=<?php  echo htmlspecialchars($carDetail->getId()); ?>' onclick="return confirm('Do you really want delete the <?php if (isset($carDetail) && !empty($carDetail) && empty($dataSearch)) echo $carDetail->getName() ?> ?');"><button type='button' class='btn btn-sm btnDelete'>Delete</button></a>
                </div>
            <?php }?>
        </div>

        <div class='col-md-6 mt-2 mb-5 allDetails'>
            <p class='text-white'><img class='iconDetailCar' src='../../Public/Icons/icons8-enter-50%20(1).png' width='20px' alt="icon of car"> This vehicle is available since : <?php echo htmlspecialchars($carDetail->getEntryDate()->format('d/m/Y')); ?></p>
            <p class='text-white' ><img class='iconDetailCar' src='../../Public/Icons/icons8-car-seat-50.png' width='20px' alt="icon of car"> Number of place : <?php  echo htmlspecialchars($carDetail->getNumberPlace());  ?> seats</p>
            <p class='text-white' ><img class='iconDetailCar' src='../../Public/Icons/icons8-fuel-gas-24.png' width='20px' alt="icon of car"> Type Of Fuel : <?php  echo htmlspecialchars($carDetail->getTypeFuel());  ?></p>
            <p class='text-white' ><img class='iconDetailCar' src='../../Public/Icons/icons8-speed-50.png' width='20px' alt="icon of car"> Power of car : <?php  echo htmlspecialchars($carDetail->getPower());  ?></p>
            <p class='text-white' ><img class='iconDetailCar' src='../../Public/Icons/icons8-color-dropper-24.png' width='20px' alt="icon of car"> Color of the car : <?php  echo htmlspecialchars($carDetail->getColor());  ?></p>
            <p class='text-white' ><img class='iconDetailCar' src='../../Public/Icons/icons8-car-badge-50.png' width='20px' alt="icon of car"> Number Plate is : <?php  echo htmlspecialchars($carDetail->getImmatriculation());  ?></p>
            <p class='text-white' ><img class='iconDetailCar' src='../../Public/Icons/icons8-wheel-26.png' width='20px' alt="icon of car"> Type of tire : <?php  echo htmlspecialchars($carDetail->getCarTire());  ?></p>


            <?php
            if($carDetail->isPowerSterring() == 1){
                echo"<p class='text-white' text-white><img class='iconDetailCar' src='../../Public/Icons/icons8-steering-wheel-50.png' width='20px' alt='icon of car'> Power Steering : Yes</p>";
            }else{
                echo"<p class='text-white' text-white><img class='iconDetailCar' src='../../Public/Icons/icons8-steering-wheel-50.png' width='20px' alt='icon of car'> Power Steering : No</p>";
            }
            ?>

            <p class='text-white' text-white><img class='iconDetailCar' src='../../Public/Icons/icons8-route-64.png' width='20px' alt="icon of car"> It has : <?php  echo htmlspecialchars($carDetail->getNbrKilometres());?> km</p>
        </div>
    </div>
    <?php
}if(isset($dataSearch) && !empty($dataSearch)){

    include_once "../Search/viewSearchCar.php";

}if(isset($carDetail) && empty($carDetail) && empty($dataSearch)){
    ?>
    <div class='row justify-content-center'>
        <div class='col-md-6 text-center'>
            <p class='mt-5 text-white'>You've not of posts for sell the cars so create it now juste click here </p>
            <a class='text-secondary text-decoration-none' href='../ViewCars/viewCreateCar.php'><button type='button' class='btn btn-dark btn-lg  ml-5 buttonSubmitAllcar'>Create my pin</button></a>
        </div>
    </div>
    <?php
}
?>

<?php include '../HeaderAndFooter/viewFooter.php' ?>
<?php include '../../Public/bootstrapJs.php' ?>

</body>
</html>
