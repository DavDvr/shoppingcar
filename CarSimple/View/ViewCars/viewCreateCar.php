<?php
ini_set('memory_limit', '1024M');
//ini_set('display_errors', 1);
require '../../vendor/autoload.php';
use App\Controllers\{CarsController}; use App\Session\{Session};use App\Security\{ValidationDataCars};

$session = new Session();
//I want all the cat
$ctrlCar = new CarsController();$findAllcat= $ctrlCar->findAllCat();
//I want all the brand
$ctrlBrand = new CarsController();$findAllBrand= $ctrlBrand->findAllBrand();
// I take the data of car for give the data to display in my form if error
$takeDataCar = new ValidationDataCars();$carsData = $takeDataCar->recupDataCar();
//i verify if i've error for display the data form
$error = $takeDataCar->isValidDataCar($carsData,$findAllcat,$findAllBrand);

//i call CarsController and i make a treatment of my data of car
$ctrlTreatment = new CarsController(); $ctrlTreatment->treatment($findAllcat,$findAllBrand);
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../Public/StyleMinify.css">
    <?php include '../../Public/bootstrap.php' ?>
    <title>Create your car</title>
</head>
<body class="idColor">

<?php include '../HeaderAndFooter/viewHeader.php' ?>
<?php if(isset($error) && count($error) > 0) $session->flashDanger();?>
<div onclick="redirection()"><?php $session->flashSuccess(); ?></div>
<script>function redirection() {setTimeout(function(){window.location.href = '../../View/AccueilAndAllCars/viewAllCar.php';}, 1000);}</script>

            <legend class="text-center tittle">Create your Car</legend>
<?php
if(isset($findAllBrand) && !empty($findAllBrand) && isset($findAllcat) && !empty($findAllcat)){
include_once '../ViewCars/Forms/formCreateCar.php';
}
?>
<br>
<br>
<br>
<br>
<br>
<?php include '../HeaderAndFooter/viewFooter.php' ?>
<?php include '../../Public/bootstrapJs.php'?>
</body>
</html>