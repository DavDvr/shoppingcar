<form action="" method="post" class="mb-5">
    <div class="container mb-5">
        <fieldset>
            <div class="container">

                <div class="row justify-content-center ">
                    <div class="form-group col-sm-6">
                        <label class="mb-2 mt-2" for="nameCat">Name Category</label>
                        <input type="text" class="form-control" name="nameCat" id="name" value="<?php if(isset($catData) && isset($error) && !empty($error)) echo htmlspecialchars($catData->getNameCategory()); else  ?>" placeholder="Enter the name of Category">
                    </div>
                </div>
                <br>
                <div class="row justify-content-center">
                    <div class="form-group col-sm-6 ">
                        <label class="mb-2 mt-1" for="DescriptionCat">Description Category</label>
                        <textarea class="form-control" id="DescriptionCat"  name="descriptionCat" rows="3"  placeholder="Enter your Description"></textarea>
                    </div>
                </div>
                <br>
                <div class="container">
                    <div class="row text-center">
                        <div class="form-group">
                            <button type="submit" name="btnSubmitCat" class="btn btn-dark btn-lg btn-block col-sm-6 mt-3 btnCat buttonSubmit">Create your Category</button>
                        </div>
                    </div>
                </div>
                <br>

            </div>
        </fieldset>
    </div>
</form>
