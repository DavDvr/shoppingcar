<form  action="" method="post" class="mb-5">
    <div class="container mb-5">
        <fieldset>
            <div class="container">
                <div class="row">
                    <div class="form-group col-sm-6">
                        <label class="mb-2"  for="nameBrand">Name Brand</label>
                        <input type="text" class="form-control" name="brand" id="nameBrand" value="<?php if(isset($dataBrand) && !empty($error)) echo htmlspecialchars($dataBrand->getName()); else ?>" placeholder="Enter the name of Brand">
                    </div>
                    <div class="form-group col-sm-6">
                        <label class="mb-2"  for="Manufacturing">Manufacturing Place</label>
                        <input type="text" class="form-control" name="manufacturing_place" id="Manufacturing" value="<?php if(isset($dataBrand) && !empty($error)) echo htmlspecialchars($dataBrand->getCarManufacturingPlace());else ?>" placeholder="Enter the Manufacturing place">
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="form-group col-sm-6">
                        <label for="exampleFormControlTextarea1">Description Brand</label>
                        <textarea class="form-control" id="exampleFormControlTextarea1" name="description" rows="3"  placeholder="Enter your Description"></textarea>
                    </div>
                    <div class="form-group col-sm-6 mt-5">
                        <input type="radio" id="Yes" name="controlquality"
                            <?php if(isset($dataBrand) && !empty($error) && $dataBrand->getControlQuality() == 1) {
                                echo "checked ";
                            }else ?> value=1>
                        <label for="Yes">Yes i have a control quality</label><br>

                        <input type="radio" id="No" name="controlquality"
                            <?php if(isset($dataBrand) && !empty($error) && $dataBrand->getControlQuality() == 0){
                                echo "checked ";
                            }else ?> value=0>
                        <label for="No">No i haven't of control quality</label><br>
                    </div>
                </div>
            </div>
            <br>
            <div class="container">
                <div class="row">
                    <div class="form-group col-sm-12">
                        <button type="submit" name="btnSubmitBrand" class="btn btn-dark btn-lg btn-block col-sm-12 mt-3 buttonSubmit">Create your Brand</button>
                    </div>
                </div>
            </div>
        </fieldset>
    </div>
</form>
