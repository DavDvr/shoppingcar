<form action="" method="post" class="mb-5" enctype="multipart/form-data">
    <div class="container mb-5">
        <fieldset>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="form-group col-sm-6 mt-4">
                                <label class="mb-2" for='exampleSelect1'>Choose your Category</label>
                                <select class='form-control sizeOption' name="catCar" id='exampleSelect1'>
                                    <option  value="" disabled selected>Select your Category</option>
                                    <?php
                                    if(isset($findAllcat)){
                                        $i=1;
                                        while($i < count($findAllcat)){
                                            $isSelected= isset($car) && $car->getIdCat() == $findAllcat[$i]->getId() ? "selected" : "";
                                            echo "<option value='" . $findAllcat[$i]->getId() . "' " . $isSelected . ">" . $findAllcat[$i]->getNameCategory() . "</option>";
                                            $i++;
                                        }
                                    }
                                    ?>
                                </select>
                                <div class="mt-1 redirectCreateCat">
                                    <p class="text-muted textPosition">You not find your category <button class="btn btn-sm buttonCreateCategory"><a class="designBntCreateCat" href='../ViewCars/viewCreateCategory.php'>Create it</a></button></p>
                                </div>
                            </div>

                            <div class="form-group col-sm-6 mt-4">
                                    <label for="brand" class="mb-2" >Choose your Brand</label>
                                    <select class="form-control sizeOption" name="brandCar" id="brand">
                                        <option value="" disabled selected>Select your Brand</option>
                                        <?php
                                        if(isset($findAllBrand)){
                                            $i=0;
                                            while($i < count($findAllBrand)){
                                                $isSelected= isset($car) && $car->getIdBrand() == $findAllBrand[$i]->getId() ? "selected" : "";
                                                echo "<option value='" . $findAllBrand[$i]->getId() . "' " . $isSelected . ">" . $findAllBrand[$i]->getName(). "</option>";
                                                $i++;
                                            }
                                        }
                                        ?>
                                    </select>
                                <div class="mt-1 redirectCreateBrand">
                                    <p class="text-muted textPosition1">You not find your brand<button class="btn btn-sm buttonCreateCategory"><a class="designBntCreateCat" href='../ViewCars/viewCreateBrand.php'>Create it</a></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-sm-6">
                        <label class="mb-2 mt-1" for="name">Name Car</label>
                        <input type="text" class="form-control nameCar" name="nameCar" id="name" value="<?php if (isset($carsData) && !empty($error)) echo htmlspecialchars($carsData->getName()); else echo ""; ?>" placeholder="Enter a name">
                    </div>
                    <div class="form-group col-sm-6">
                        <label class="mb-2 mt-1" for="numberPlace">Number Place</label>
                        <input type="number" class="form-control" name="nbrPlaceCar" id="numberPlace" value="<?php if (isset($carsData) && !empty($error)) echo htmlspecialchars($carsData->getNumberPlace()); else echo "";?>" placeholder="Enter a number">
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="form-group col-sm-6">
                        <label class="mb-2" for="power">The Car Power</label>
                        <input type="text" class="form-control" name="powerCar" id="power"  value="<?php if (isset($carsData) &&  !empty($error)) echo htmlspecialchars($carsData->getPower()); else echo "";?>" placeholder="Enter your car power">
                    </div>
                    <div class="form-group col-sm-6">
                        <label class="mb-2" for="fuel">Type Fuel</label>
                        <input type="text" class="form-control" name="fuelCar" id="fuel" value="<?php if (isset($carsData) &&  !empty($error)) echo htmlspecialchars($carsData->getTypeFuel()); else echo "";?>" placeholder="Enter your fuel">
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="form-group col-sm-4">
                        <label class="mb-2" for="color">The Color</label>
                        <input type="text" class="form-control" name="colorCar" id="color" value="<?php if (isset($carsData) &&  !empty($error)) echo htmlspecialchars($carsData->getColor()); else echo "";?>" placeholder="Enter your color">
                    </div>
                    <div class="form-group col-sm-4">
                        <label class="mb-2" for="plate">Numberplate</label>
                        <input type="text" class="form-control" name="nbrPlateCar" id="plate" value="<?php if (isset($carsData) &&  !empty($error)) echo htmlspecialchars($carsData->getImmatriculation()); else echo "";?>" placeholder="Enter your number plate">
                    </div>
                    <div class="form-group col-sm-4">
                        <label class="mb-2" for="plate">Number of Kilometers</label>
                        <input type="text" class="form-control " name="nbrKilometersCar" id="plate" value="<?php if (isset($carsData) &&  !empty($error)) echo htmlspecialchars($carsData->getNbrKilometres()); else echo "";?>" placeholder="Enter your number of kilometers">
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="form-group col-sm-2">
                        <label class="mb-2" for="tire">Car tire</label>
                        <input type="text" class="form-control" name="tireCar" id="tire" value="<?php if (isset($carsData) &&  !empty($error)) echo htmlspecialchars($carsData->getCarTire()); else echo "";?>" placeholder="Enter your tire">
                    </div>
                    <div class="form-group col-sm-2">
                        <label class="mb-2" for="price">Price</label>
                        <input type="number" class="form-control" name="priceCar" step="0.01" id="price" value="<?php if (isset($carsData) &&  !empty($error)) echo htmlspecialchars($carsData->getPrice()); else echo "";?>" placeholder="Enter your price">
                    </div>
                    <div class="form-group col-sm-4">
                        <label class="form-label" for="customFile">Upload your image</label>
                        <input type="file" class="form-control" name="imgCar" id="customFile"  value="<?php if (isset($carsData) &&  !empty($error)) echo htmlspecialchars($carsData->getNameFile()); else echo "";?>"/>
                    </div>
                    <div class="form-group col-sm-4 mt-4">
                        <input type="radio" id="Yes" name="powersterringCar"
                        <?php
                        if (isset($carsData) &&  !empty($error) && $carsData->isPowerSterring() == 1){
                            echo "checked";
                        }else{
                        echo "";
                        }
                        ?> value= 1>
                        <label for="Yes">Yes it's with power steering</label><br>
                        <input type="radio" id="No" name="powersterringCar"
                       <?php
                       if (isset($carsData) &&  !empty($error) && $carsData->isPowerSterring() == 0){
                            echo "checked";
                        }else{
                           echo "";
                       }
                        ?>value= 0>
                        <label for="No">No it's not power steering</label><br>

                    </div>

                </div>

                <br>
                <button type="submit" name="btnSubmitCar" class="btn btn-dark btn-lg btn-block col-sm-12 mt-3 btnCar buttonSubmit">Create your Car</button>
            </div>
        </fieldset>
    </div>
</form>

