<?php
//ini_set('display_errors', 1);
require '../../vendor/autoload.php';
use App\Controllers\{CategoryController};
use App\Security\{ValidationDataCategory};
use App\Session\{Session};
$session = new Session();

$crtlCat = new CategoryController();
$crtlCat->treatmentCategory();
$recupCat = new ValidationDataCategory(); $catData = $recupCat->recupDataCategory();
$error =$recupCat->validDataCategory($catData);

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../Public/StyleMinify.css">
    <?php include '../../Public/bootstrap.php' ?>
    <title>Create your Category</title>
</head>
<body class="idColor">
<?php include '../HeaderAndFooter/viewHeader.php' ?>

<?php if(isset($error) && count($error) > 0 ) $session->flashDanger();?>
<?php $session->flashSuccess();?>

<legend class="text-center tittleCAt mt-5 mb-5 ">Create your Category</legend>
<?php
echo"<div class='col-md-2'>";
    echo"<a href='../ViewCars/viewCreateCar.php'><button type='button' class='btn btn-dark createCat'>Create My Car</button></a>";
    echo"</div>";
?>
<?php include_once '../ViewCars/Forms/formCreateCategory.php' ?>

<?php include '../HeaderAndFooter/viewFooter.php' ?>
<?php include '../../Public/bootstrapJs.php' ?>
</body>
</html>
