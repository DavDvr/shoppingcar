<?php
//ini_set('display_errors', 1);
require'../../vendor/autoload.php';
use App\Session\{Session};
use App\Controllers\{BrandController,CarsController};
use App\Security\{ValidationDataBrand};
$session = new Session();

$crtlBrand = new BrandController();
$crtlBrand->treatmentBrand();

$recupBrand = new ValidationDataBrand();
$dataBrand= $recupBrand->recupDataBrand();
$error= $recupBrand->isValidBrandData($dataBrand);
$controllerCar= new CarsController();
$admin =$controllerCar->getAdmin();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../Public/StyleMinify.css">
    <?php include '../../Public/bootstrap.php'; ?>
    <title>Create your Brand</title>
</head>
<body class="idColor">
<?php include '../HeaderAndFooter/viewHeader.php'; ?>

<?php if(isset($error) && count($error) > 0 ) $session->flashDanger();?>
<?php $session->flashSuccess();?>
<legend class="text-center tittleBrand mb-5 mt-4">Create your Brand</legend>
<?php
echo"<div class='col-md-2'>";
echo"<a href='../ViewCars/viewCreateCar.php'><button type='button' class='btn btn-dark createCat'>Create My Car</button></a>";
echo"</div>";
?>
<?php include_once '../ViewCars/Forms/formCreateBrand.php';?>

<?php include '../HeaderAndFooter/viewFooter.php'; ?>
<?php include '../../Public/bootstrapJs.php'; ?>
</body>
</html>
