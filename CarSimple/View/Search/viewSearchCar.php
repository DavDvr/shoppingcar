<?php
echo '<body class="idColor overflow">';

echo' <h1 class="text-center h2 mt-5 mb-4 text-white titleSearch">Your Search Of Cars</h1>';

echo"<div class='col-md-2 '>";
echo "<a href='../AccueilAndAllCars/viewAllCar.php'><button type='button' class='btn btn-dark GoBackAllCars'>Go Back</button></a>";
echo"</div>";

echo '<div class="row mx-auto rowBottomSearch">';
echo'<br>';
foreach ($dataSearch as $car) {
    echo '<div class="col-md-3">';
    echo '<article class="bg-white rounded shadow ">';

    echo "<div class='text-center'>";
    echo" <h1 class='titleCat text-uppercase'>";
    if (isset($car)) echo htmlspecialchars($daoCat->findById($car->getIdCat())->getNameCategory());
    echo"</h1>";
    echo  "</div>";

    echo "<div class='shadow p-1  bg-white rounded-lg overflow-hidden'>";
    echo"<a href='../ViewDetailCars/viewDetailCars.php?idCar={$car->getId()}'>";
    echo "<img class='w-100 imageEffect rounded object-fit-cover' src=".htmlspecialchars($car->getFile()); echo">";
    echo "</a>";
    echo"</div>";
    echo"<p class='PriceCar text-center mt-2'>".htmlspecialchars($car->getPrice());echo " €</p>";


    echo" <h1 class='text-muted text-left titleName'>";
    if (isset($car)) echo htmlspecialchars($car->getName());
    echo"</h1>";


    echo "<a class='text-secondary text-decoration-none' href='../ViewDetailCars/viewDetailCars.php?idCar={$car->getId()}'><button type='button' class='btn btn-dark btn-lg col-sm-12 mr-2 ml-2 mt-1 buttonSubmitAllcar queryButton'>Show details</button></a>";

    echo '</article>';

    echo '</div>';
}

echo'</div>';
?>