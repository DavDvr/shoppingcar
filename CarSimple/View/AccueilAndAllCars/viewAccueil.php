<?php
session_start();
//ini_set('display_errors', 1);
require '../../vendor/autoload.php';
use App\controllers\{SearchController};
use App\Dao\{DaoCars,DaoCategory};
$daoCat = new DaoCategory();
//acces controller search bar for display the search of cars
$searchContoler = new SearchController();
$dataSearch = $searchContoler->treatment();
$daoCar = new DaoCars();
$car = $daoCar->findById(82);
$car2 = $daoCar->findById(73);
$car3 = $daoCar->findById(75);

?>
<!doctype html>
<html class="hideOverflow" lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../Public/StyleMinify.css">
    <?php include '../../Public/bootstrap.php' ?>
    <title>Accueil</title>
</head>
<?php include '../HeaderAndFooter/viewHeader.php' ?>
<?php
if(isset($dataSearch) && !empty($dataSearch)){
    include_once "../Search/viewSearchCar.php";

?>
<?php
}elseif(isset($car)&& isset($car2) && isset($car3) && empty($dataSearch)){

echo"

<body class='idColor'>


<div id='carouselExampleControls' class='carousel slide' data-bs-ride='carousel'>
    <div class='carousel-inner'>
        <div class='carousel-item active'>
            ".(!empty($car3) ? "<a href='../ViewDetailCars/viewDetailCars.php?idCar=".$car3->getId()."'><button class='btn btn-dark' id='btnCarousel1'>See The Car</button></a><img src='../../Public/Caroussel/pexels-mike-794435.jpg' class='img-fluid' alt='Voiture de sport'>":"<img src='../../Public/Caroussel/pexels-mike-794435.jpg' class='img-fluid' alt='Voiture de sport'>")."
        </div>
        <div class='carousel-item'>
             ".(!empty($car2) ? "<a href='../ViewDetailCars/viewDetailCars.php?idCar=".$car2->getId()."'><button class='btn btn-dark' id='btnCarousel1'>See The Car</button></a><img src='../../Public/Caroussel/pexels-mike-193021.jpg' class='img-fluid' alt='Voiture de luxe'>":"<img src='../../Public/Caroussel/pexels-mike-193021.jpg' class='img-fluid' alt='Voiture de luxe'>")."
        </div>
        <div class='carousel-item'>
           ".(!empty($car) ? " <a href='../ViewDetailCars/viewDetailCars.php?idCar=".$car->getId()."'><button class='btn btn-dark' id='btnCarousel1'>See The Car</button></a><img src='../../Public/Caroussel/pexels-mike-136872.jpg' class='img-fluid' alt='Voiture de luxe'>":"<img src='../../Public/Caroussel/pexels-mike-136872.jpg' class='img-fluid' alt='Voiture de luxe'>")."
        </div>
        <div class='carousel-item'>
            <img src='../../Public/Caroussel/pexels-mike-217326.jpg' class='img-fluid' alt='Voiture de luxe'>
        </div>
    </div>
    <button class='carousel-control-prev' type='button' data-bs-target='#carouselExampleControls'  data-bs-slide='prev'>
        <span class='carousel-control-prev-icon' aria-hidden='true'></span>
        <span class='visually-hidden'>Previous</span>
    </button>
    <button class='carousel-control-next' type='button' data-bs-target='#carouselExampleControls'  data-bs-slide='next'>
        <span class='carousel-control-next-icon' aria-hidden='true'></span>
        <span class='visually-hidden'>Next</span>
    </button>
</div>";

}
?>
<?php include '../HeaderAndFooter/viewFooter.php' ?>
<?php include '../../Public/bootstrapJs.php' ?>
</body>
</html>
