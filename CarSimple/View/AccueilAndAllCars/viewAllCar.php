<?php
session_start();
ini_set('memory_limit', '1024M');
//ini_set('display_errors', 1);
require '../../vendor/autoload.php';
use App\controllers\{SearchController,CarsController};
use App\Dao\{DaoCars,DaoCategory};
use App\Session\{Session};
$daoCat = new DaoCategory();
$daoCars=new DaoCars();
$searchContoler = new SearchController();//acces controller search bar for display the search of cars
$controllerCar = new CarsController();
$session = new Session();
$valuecars = $daoCars->findAll();
$dataSearch = $searchContoler->treatment();
$idcar = filter_input(INPUT_GET,'deleteId');
$controllerCar->deleteCar($idcar);
$controllerCar->like();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../Public/StyleMinify.css">
    <?php include '../../Public/bootstrap.php' ?>
    <title>Accueil</title>
</head>
<body class="idColor overflow">
<?php include '../HeaderAndFooter/viewHeader.php' ?>
<div onclick="redirection()"><?php $session->flashSuccess(); ?></div>
<script>function redirection() {setTimeout(function(){window.location.href = '../../View/AccueilAndAllCars/viewAllCar.php';}, 1000);}</script>

<?php
if (isset($valuecars) && !empty($valuecars) && isset($daoCat) && empty($dataSearch)) {
        echo '<h1 class="text-center h3 text-white mt-5 mb-4 tittleAllCar">All the cars</h1>';

        echo '<div class="row  mx-auto rowBottom">';

                foreach ($valuecars as $cars) {
                    echo '<div class="col-md-3">';

                        echo '<article class="bg-white rounded shadow mb-4 resizeArticle">';

                            echo "<div class='text-center'>";
                                echo" <h1 class='titleCat text-uppercase'>";
                                    if (isset($cars)) echo htmlspecialchars($daoCat->findById($cars->getIdCat())->getNameCategory());
                                echo"</h1>";
                            echo  "</div>";

                            echo "<div class='shadow p-1  bg-white rounded-lg overflow-hidden'>";
                                echo"<a href='../ViewDetailCars/viewDetailCars.php?idCar={$cars->getId()}'>";
                                    echo "<img class='w-100 imageEffect rounded object-fit-cover' alt='image of car' src=".htmlspecialchars($cars->getFile()); echo">";
                                echo "</a>";
                            echo"</div>";
                            echo"<p class='PriceCar text-center mt-2'>".htmlspecialchars($cars->getPrice());echo " €</p>";

                            echo" <h1 class='text-muted text-left titleName'>";
                                    if (isset($cars)) echo htmlspecialchars($cars->getName());
                            echo"</h1>";

                    echo"<button class='rounded-circle border border-white bg-white btnHeart'><a id='colorHeart' class='heart' href='../AccueilAndAllCars/viewAllCar.php?type=1&id={$cars->getId()}'>&hearts;</a></button><span class='textLike'>Like : ".$controllerCar->displayLike($cars->getId())."</span>";

                    echo "<a class='text-secondary text-decoration-none' href='../ViewDetailCars/viewDetailCars.php?idCar={$cars->getId()}'><button type='button' class='btn btn-dark btn-lg col-sm-12 mr-2 ml-2 buttonSubmitAllcar'>Show details</button></a>";
                        echo '</article>';
                    echo '</div>';
                }
        echo'</div>';



}if(isset($dataSearch) && !empty($dataSearch)){
    include_once "../Search/viewSearchCar.php";

}if(isset($valuecars) && empty($valuecars) && isset($daoCat) && empty($dataSearch)){
    echo "<div class='row justify-content-center'>";
        echo "<div class='col-md-6 text-center mt-5'>";
            echo "<p class='mt-5 text-white'>You've not of posts for sell the cars so create it now juste click here </p>";
            echo '<a class=" text-secondary text-decoration-none" href="../ViewCars/viewCreateCar.php"><button type="button" class="btn btn-dark btn-lg  ml-5 buttonSubmitAllcar">Create my pin</button></a>';
        echo '</div>';
    echo '</div>';
}

?>

<?php include '../HeaderAndFooter/viewFooter.php' ?>
<?php include '../../Public/bootstrapJs.php' ?>

</body>
</html>
