<?php
require '../../vendor/autoload.php';
use App\Controllers\{CarsController};
$contollerCar = new CarsController();
$admin = $contollerCar->getAdmin();
var_dump($admin);
?>
<header class="fixed-top">
    <nav class="navbar navbar-expand-lg navbar-light  colorNav">
        <div class="container-fluid">
            <a class="navbar-brand" href="../AccueilAndAllCars/viewAccueil.php">CarShopping</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav">
                     <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="../AccueilAndAllCars/viewAccueil.php">Home</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link"  href="../AccueilAndAllCars/viewAllCar.php">Cars</a>
                     </li>
                     <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Access Customers
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <li>
                                <a class="dropdown-item" href="../Account/viewFormAccount.php">Create Account</a>
                            </li>
                            <?php if(!isset($_SESSION['auth'])){ echo "
                            <li>
                                <a class='dropdown-item' href='../Logged/viewFormConnect.php'>Connect</a>
                            </li>";
                            }?>
                            <li>
                                <a class="dropdown-item" href="#">Account</a>
                            </li>
                            <li class="nav-item">
                                <?php if(isset($_SESSION['auth'])) echo "<a class='dropdown-item' href='../Logged/logout.php'>Logout</a>" ?>
                            </li>
                        </ul>
                     </li>
                    <?php if(isset($admin))
                     echo '<li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenu" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                           Access Admin
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenu">
                            <li><a  class="dropdown-item" href="../ViewCars/viewCreateCar.php">Create Cars</a></li>
                            <li><a class="dropdown-item" href="../ViewCars/viewCreateCategory.php">Create Category</a></li>
                            <li><a class="dropdown-item" href="../ViewCars/viewCreateBrand.php">Create Brand</a></li>
                        </ul>
                     </li>';
                    ?>
                </ul>
                    <?php include '../Search/Form/formSearchCar.php';?>

                    <p class="nav-link">
                        <p class="userConnected"> <?php if(isset($_SESSION['auth'])) echo " ".$_SESSION['auth'] . " <img class='IconUser' src='../../Public/Icons/icons8-checked-user-male-26.png' alt='icon user logged'>" ?></p>
            </div>
        </div>
    </nav>
</header>