<footer class="fixed-bottom">
    <div class="card text-center border-0">
        <div class="card-body footerColor">
            <a href="../AccueilAndAllCars/viewAccueil.php" class="btn btn-dark mt-2 btnFooter">Go Back Home</a>
            <p class="copyright mt-1">Website by @copyright David Devaure</p>
        </div>

    </div>
</footer>

