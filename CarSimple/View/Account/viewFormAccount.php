<?php
//ini_set('display_errors', 1);
require '../../vendor/autoload.php';
use App\Session\{Session};
use App\Controllers\{AccountController};
Use App\Security\{ValidationDataCustomer};
$session = new Session();

$crtlAccount = new AccountController();

$crtlAccount->treatmentAccount();
$dataCustomer = new ValidationDataCustomer();
$customer = $dataCustomer->recupDataCustomer();
$error = $dataCustomer->isValidDataInscription($customer);

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../../Public/StyleMinify.css">
    <?php include '../../Public/bootstrap.php' ?>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Create Account</title>
</head>
<body class="idColor">
<?php include '../HeaderAndFooter/viewHeader.php' ?>
<?php if(isset($error) && count($error)>0) $session->flashAccountDanger();?>
<span onclick="redirection()"><?php $session->flashAccountSuccess();?></span>
<script>function redirection() {setTimeout(function(){window.location.href = '../../View/Logged/viewFormConnect.php';}, 1000);}</script>

<legend class="text-center  tittleAccount">Create your Account</legend>
<form method="post" class="formAccount">
    <div class="container">
        <fieldset>
            <div class="container">
                <div class="row">
                    <div class="form-group col-sm-6">
                        <label class="mb-2 mt-5" for="name">LastName</label>
                        <input type="text"  class="form-control" id="name" name="lastname" value="<?php if(isset($customer) && !empty($error)) echo htmlspecialchars($customer->getLastName()); else echo ""; ?>" placeholder="Enter your name">
                    </div>
                    <div class="form-group col-sm-6">
                        <label class="mb-2 mt-5" for="firstname">FirstName</label>
                        <input type="text"   class="form-control" id="firstname" name="firstname" value="<?php if(isset($customer) && !empty($error)) echo htmlspecialchars($customer->getFirstName()); else echo ""; ?>" placeholder="Enter your firstName">
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="form-group col-sm-2">
                        <label class="mb-2" for="birthdate">Your Birthdate</label>
                        <input type="date" class="form-control" id="birthdate" name="birthdate" value="<?php if(isset($customer) && !empty($error)) echo htmlspecialchars($customer->getBirthdate()); else echo ""; ?>" placeholder="Enter your Birthdate">
                    </div>
                    <div class="form-group col-sm-5">
                        <label class="mb-2" for="licence">Your Licence Number</label>
                        <input type="text" class="form-control" id="licence" name="licencenumber" value="<?php if(isset($customer) && !empty($error)) echo htmlspecialchars($customer->getLicenceNumber()); else echo ""; ?>" placeholder="Enter your Licence Number">
                    </div>
                    <div class="form-group col-sm-5">
                        <label class="mb-2" for="address">Your Adresse</label>
                        <input type="text" class="form-control" id="address" name="address" value="<?php if(isset($customer) && !empty($error)) echo htmlspecialchars($customer->getAddress()); else echo ""; ?>" placeholder="Enter your Adresse">
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="form-group col-sm-6">
                        <label class="mb-2" for="password">Your Password</label>
                        <input type="password"  class="form-control" id="password" name="password" placeholder="Enter your Password">
                    </div>
                    <div class="form-group col-sm-6">
                        <label class="mb-2" for="email">Your email</label>
                        <input type="email" class="form-control" id="email" name="email" value="<?php if(isset($customer) && !empty($error)) echo htmlspecialchars($customer->getEmail()); else echo ""; ?>" placeholder="Enter your Email">
                    </div>

                </div>
                <button type="submit" name="btn-submit" class="btn btn-dark btn-lg tbn-block col-sm-12 mt-5 btnAccount buttonSubmit">Create my Account</button>
                <?php
                if(!isset($_SESSION['auth'])){
                 echo"<div class='text-muted redirectionConnect'>You've already account, you can connect here<button class='btn btn-dark btn-sm buttonConnectMe'><a class='designButton' href='../Logged/viewFormConnect.php'>Connect Me</a></button></div>";
                }
                ?>
            </div>
        </fieldset>
    </div>
</form>
<?php include '../HeaderAndFooter/viewFooter.php' ?>
<?php include '../../Public/bootstrapJs.php' ?>
</body>
</html>
