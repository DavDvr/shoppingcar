<?php
//ini_set('display_errors', 1);
require '../../vendor/autoload.php';

use App\Session\{Session};
use App\Controllers\{LoggedController};
use App\Security\{ValidationDataCustomer};
$session = new Session();
$ctrlFindCustomer = new LoggedController();
$ctrlLogged = new LoggedController();
$ctrlLogged->logged();
$error = $ctrlLogged->isErrorLoggin();
$validationData = new ValidationDataCustomer();
$dataLogin = $validationData->recupDataForLogin();
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../Public/StyleMinify.css">
    <?php include '../../Public/bootstrap.php' ?>
    <title>Connect</title>
</head>
<body class="idColor">
<?php include '../HeaderAndFooter/viewHeader.php' ?>
<?php if(isset($error) && count($error) > 0 ) $session->flashDanger();?>
<div onclick="redirection()"><?php $session->flashSuccess();?></div>
<script>function redirection() {setTimeout(function(){window.location.href = '../../View/AccueilAndAllCars/viewAccueil.php';}, 1000);}</script>
<legend class="text-center tittleLogin mb-3 mt-2">You Can Connect Here</legend>
<form action="" method="post" class="mb-5">
    <div class="container mb-5">
        <fieldset>
            <div class="container">
                <div class="row justify-content-center ">
                    <div class="form-group col-sm-6">
                        <label class="mb-2" for="emailLog">Your Email</label>
                        <input type="email" class="form-control" id="emailLog" name="emailLogin" value="<?php if (isset($dataLogin) && isset($error) && !empty($error)) echo htmlspecialchars($dataLogin->getEmail()); else echo ""; ?>" placeholder="Enter your email">
                    </div>
                </div>
                <br>
                <div class="row justify-content-center">
                    <div class="form-group col-sm-6">
                        <label class="mb-2" for="password">Your password</label>
                        <input type="password" class="form-control " id="password" name="passwordLogin" placeholder="Enter your password">
                    </div>
                </div>
                <br>
                <div class="row justify-content-center">
                    <div class="form-group col-sm-6">
                        <label class="mb-2" for="confirmPassword">Confirmation password</label>
                        <input type="password" class="form-control " id="confirmPassword" name="confirmationPassword" placeholder="Enter your confirmation password">
                    </div>
                </div>
                <br>
                <div class="container">
                    <div class="row">
                        <div class="col text-center">
                        <button type="submit" name="btnSubmit" class="btn btn-dark btn-lg btn-block col-sm-6 mt-2 buttonSubmit buttonConnect ml-5">Connect Me</button>
                        </div>
                        <div class="text-muted redirectionAccount">You've not already account, you can create it here<button class="btn btn-dark btn-sm buttonCreateAccount"><a class="designButtonAccount" href="../Account/viewFormAccount.php">Create Account</a></button></div>
                    </div>
                </div>
            </div>
                <br>
        </fieldset>
    </div>
</form>

<?php include '../HeaderAndFooter/viewFooter.php' ?>
<?php include '../../Public/bootstrapJs.php' ?>
</body>
</html>
