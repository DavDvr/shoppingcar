<?php
require'../../vendor/autoload.php';
ini_set('memory_limit', '1024M');
//ini_set('display_errors', 1);

use App\Controllers\{UpdatecarController};
use App\Security\{ValidationUpdateCar};
use App\Session\{Session};
$session = new Session();

$crtlFindById = new UpdatecarController(); $car= $crtlFindById->findById();

//I want all the cat
$ctrlUpdateCat = new UpdatecarController(); $findAllcat= $ctrlUpdateCat->findAllCat();
//I want all the brand
$ctrlUpdateBrand = new UpdatecarController();$findAllBrand= $ctrlUpdateBrand->findAllBrand();
// I take the data of car for give the data to display in my form if error
$takeDataCar = new ValidationUpdateCar();$carsData = $takeDataCar->recupDataUpdateCar();
//i verify if i've error for display the data form
$error = $takeDataCar->isValidUpdateDataCar($carsData,$findAllcat,$findAllBrand);

//i call CarsController and i make a treatment of my data of car
$ctrlTreatmentUpdate = new UpdatecarController(); $ctrlTreatmentUpdate->treatmentUpdateCar($findAllcat,$findAllBrand);

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../Public/StyleMinify.css">
    <?php include '../../Public/bootstrap.php' ?>
    <title>Update your car</title>
</head>
<body class="idColor">

<?php include '../HeaderAndFooter/viewHeader.php' ?>
<?php if(isset($error) && count($error) > 0) $session->flashDanger();else?>
<div onclick="redirectionUpdate()"><?php $session->flashSuccess(); ?></div>
<script>function redirectionUpdate() {setTimeout(function(){window.location.href= '../../View/AccueilAndAllCars/viewAllCar.php';}, 1000);}</script>
<br>
<legend class="text-center tittle">Update your Car</legend>

<?php
if(isset($findAllBrand) && !empty($findAllBrand) && isset($findAllcat) && !empty($findAllcat) && isset($car) && !empty($car)){
    include_once 'Form/formUpdate.php';
}
?>
<br>
<br>
<br>
<br>
<br>
<?php include '../HeaderAndFooter/viewFooter.php' ?>
<?php include '../../Public/bootstrapJs.php'?>
</body>
</html>